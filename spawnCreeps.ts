/**
 * responsible for spawning new creeps
 * called once for every room.
 *
 * @param roomCreeps
 * @param roomSpawn
 * @param room
 */
import {countCreepsFromArray, creepCountsLessThan} from "./utils/lib.creep";
import {PRIORITY_MUSTER} from "./goalPriorities";
import {BehaviorGoToRoom} from "./Behaviors";

const requestBodies: { [key: string]: BodyPartConstant[][] } =
    {
        Carrier:
            [
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY],
                [MOVE, MOVE, MOVE, CARRY, CARRY, CARRY],
            ],
        Soldier:
            [
                [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK],
                [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK],
                [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK],
                [TOUGH, TOUGH, TOUGH, ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE],
                [TOUGH, TOUGH, ATTACK, ATTACK, MOVE, MOVE, MOVE, MOVE],
                [ATTACK, ATTACK, MOVE, MOVE],
                [ATTACK, MOVE]
            ],
        Claimer:
            [
                [TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, CLAIM, CLAIM, CLAIM],
                [TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, CLAIM, CLAIM],
                [MOVE, MOVE, CLAIM, CLAIM],
                [ATTACK, CLAIM, MOVE, MOVE],
                [CLAIM, MOVE],
            ],
        Builder:
            [
                [MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE],
                [WORK, WORK, WORK, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY],
                [WORK, WORK, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY],
                [WORK, MOVE, CARRY],
            ]
    };

export function spawnCreeps(roomUnspecializedCreeps: Creep[], roomSpawn: StructureSpawn, room: Room)
{
    //don't run if the room has no spawn
    if (!roomSpawn)
    {
        if (room.memory.logSettings.debug)
            console.log(Game.time + " [" + room.name + "] has no spawn!");
        return;
    }

    //make sure spawn has required memory objects
    if (!roomSpawn.memory.unmetPreferences)
        roomSpawn.memory.unmetPreferences = [];

    //spawn variables
    let requestedBody: BodyPartConstant[] | null = null;
    let spawnWasRequested: boolean = false;
    let spawnWasForRemoteRoom: boolean = false;
    let newCreepMemory: CreepMemory = {};

    if (roomSpawn.memory.unmetPreferences && roomSpawn.memory.unmetPreferences.length > 0)
    {
        //handle requests from goals with unmet preferences
        const requestType: string = roomSpawn.memory.unmetPreferences[0];
        if (!requestBodies[requestType])
        {
            console.log(Game.time + " [" + room.name + "][WARN] a unit of type " + requestType + " was requested, but no bodies could be found for that!");
        }
        else
        {
            //find the cheapest body of the requested type we can afford
            requestedBody = _.find(requestBodies[requestType], body =>
            {
                const bodyCost = body.reduce((cost, part) => cost + BODYPART_COST[part], 0);
                return bodyCost <= room.energyAvailable;
            }) || null;

            //spawn that
            if (requestedBody)
                spawnWasRequested = true;
        }
    }
    else if (room.find(FIND_HOSTILE_CREEPS).length > 0)
    {
        //room is under attack!
        //find the cheapest soldier of the requested type we can afford
        requestedBody = _.find(requestBodies["Soldier"], body =>
        {
            const bodyCost = body.reduce((cost, part) => cost + BODYPART_COST[part], 0);
            return bodyCost <= room.energyAvailable;
        }) || null;

        //spawn that
        if (requestedBody)
            spawnWasRequested = true;
    }
    else if (creepCountsLessThan(countCreepsFromArray(roomUnspecializedCreeps), room.memory.spawnSettings.targetCreepCounts))
    {
        //spawn new generic units if there are less than room settings dictate
        //we want the largest option we can afford, but if the creep count is low we allow it to drop down
        //to avoid stalling out at low energy
        // if (room.energyCapacityAvailable >= 2300 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 2300))
        //     requestedBody = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, CARRY];
        // if (room.energyCapacityAvailable >= 2000 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 2000))
        //     requestedBody = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, CARRY];
        if (room.energyCapacityAvailable >= 1450 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 1450))
            requestedBody = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY];
        if (room.energyCapacityAvailable >= 1300 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 1300))
            requestedBody = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY];
        if (room.energyCapacityAvailable >= 1000 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 1000))
            requestedBody = [MOVE, MOVE, MOVE, MOVE, MOVE, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE];
        else if (room.energyCapacityAvailable >= 750 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 750))
            requestedBody = [WORK, WORK, WORK, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY];
        else if (room.energyCapacityAvailable >= 500 && (roomUnspecializedCreeps.length > 3 || room.energyAvailable >= 500))
            requestedBody = [WORK, WORK, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY];
        else
            requestedBody = [WORK, MOVE, CARRY];
    }
    else if (roomSpawn.memory.remoteSpawnRequests && roomSpawn.memory.remoteSpawnRequests.length > 0 && ((room.energyAvailable / room.energyCapacityAvailable) > 0.8))
    {
        //the room has its own needs covered, but a remote room is asking for help.  Spawn something for them
        const request: RemoteSpawnRequest = roomSpawn.memory.remoteSpawnRequests[0];
        const requestType = (Memory.flags[request.requestingFlagName] || {}).requestingCreepType;

        if (!requestType)
        {
            roomSpawn.memory.remoteSpawnRequests.splice(0, 1);
        }
        else if (!requestBodies[requestType])
        {
            console.log(Game.time + " [" + room.name + "][WARN] a unit of type " + requestType + " was requested, but no bodies could be found for that!");
        }
        else
        {
            //find the first body of the requested type we can afford
            requestedBody = _.find(requestBodies[requestType], body =>
            {
                const bodyCost = body.reduce((cost, part) => cost + BODYPART_COST[part], 0);
                return bodyCost <= room.energyAvailable;
            }) || null;

            //spawn that
            if (requestedBody)
            {
                spawnWasRequested = true;

                //if the request came with a memory object, use that.  otherwise, just instruct the new creep to go to the requesting room.
                if (Game.flags[request.requestingFlagName].memory.requestingCreepMemory)
                    newCreepMemory = Game.flags[request.requestingFlagName].memory.requestingCreepMemory!;
                else
                {
                    newCreepMemory = {
                        goal:
                            {
                                targetId: Game.flags[request.requestingFlagName].pos.roomName,
                                priority: PRIORITY_MUSTER,
                                name: "Move to assist room",
                                behaviorName: BehaviorGoToRoom.name,
                                partsRequested: [],
                                pos: roomSpawn.pos, //we don't know what the actual position is, since it is an entire room, but this behavior ignores the position anyway, so we cna just pass a placeholder
                            },
                        specialized: true,
                    };
                }

                spawnWasForRemoteRoom = true;
            }
        }
    }

    if (room.memory.logSettings.spawning)
    {
        if (roomSpawn.spawning)
            console.log(Game.time + " [" + room.name + "] new unit in " + roomSpawn.spawning.remainingTime);
        else if (room.memory.logSettings.spawning)
            console.log(Game.time + " [" + room.name + "] next spawn will be: " + JSON.stringify(requestedBody));
        else
            console.log(Game.time + " [" + room.name + "] no spawns needed");
    }

    if (!roomSpawn.spawning && requestedBody && requestedBody.length > 0)
    {
        //if the requested creep has no WORK parts, force it to be a "specialized" unit
        if (!_.contains(requestedBody, WORK))
            newCreepMemory.specialized = true;

        //store the room the creep was spawned from
        newCreepMemory.spawnedFromRoom = room.name;

        //spawn a creep with the given body and a random name
        const newCreepName = Math.random().toString(36).slice(2);
        const spawnResult = roomSpawn.spawnCreep(requestedBody, newCreepName, {memory: newCreepMemory});
        switch (spawnResult)
        {
            case OK:
                if (room.memory.logSettings.spawning)
                    console.log(Game.time + " [" + room.name + "] new spawn initiated");

                //if this spawn was from a request, remove it from the queue
                if (spawnWasRequested)
                    roomSpawn.memory.unmetPreferences.splice(0, 1);

                //if this spawn was to assist a remote room, remove it from the queue and report it to the flag
                if (spawnWasForRemoteRoom)
                {
                    if (!Memory.flags[roomSpawn.memory.remoteSpawnRequests[0].requestingFlagName].flagCreeps)
                        Memory.flags[roomSpawn.memory.remoteSpawnRequests[0].requestingFlagName].flagCreeps = [];

                    Memory.flags[roomSpawn.memory.remoteSpawnRequests[0].requestingFlagName].flagCreeps!.push(newCreepName);

                    roomSpawn.memory.remoteSpawnRequests.splice(0, 1);
                }

                break;
            case ERR_NOT_ENOUGH_ENERGY:
                if (room.memory.logSettings.spawning)
                    console.log(Game.time + " [" + room.name + "] Can't afford new unit");
                break;
            default:
                console.log(Game.time + " [" + room.name + "] unexpected spawnResult: " + spawnResult);
                break;
        }
    }
}
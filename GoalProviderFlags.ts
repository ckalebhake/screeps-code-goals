import {
    BehaviorCollectFromLab,
    BehaviorGoToPosition, BehaviorHarvestEnergy, BehaviorHarvestToContainer, BehaviorMuster,
    BehaviorSupplyLabWithMineral,
} from "./Behaviors";
import {roomContainersAndStorage} from "./utils/lib.room";
import {
    PRIORITY_IDLE} from "./goalPriorities";
import {
    PRIORITY_CLEANUP_RESOURCES, PRIORITY_COLLECT, PRIORITY_COLLECT_URGENT,
    PRIORITY_HARVEST, PRIORITY_HARVEST_EXTRA,
    PRIORITY_HARVEST_MINERAL,
    PRIORITY_MUSTER, PRIORITY_SUPPLY_LAB
} from "./goalPriorities";

/**
 * we know we should be harvesting from this source, but havent yet determined how
 * this sets the flag to a more specific flag, and then executes that behavior
 * the determination process is expensive, but only happens once.
 * @type {{evaluate(flag: Flag): Goal[]}}
 */
export const GoalProviderHarvestUnspecified : GoalProvider =
{
    evaluate(flag: Flag) : Goal[]
    {
        console.log("evaluating harvest type for flag " + flag);

        //base harvesting behavior on nearby structures
        if(!flag.room)
        {
            console.log("Removing harvest flag " + flag.name + " because the room could not be found.");
            flag.remove();
            return [];
        }

        const lookResults = flag.room.lookForAtArea(
            LOOK_STRUCTURES,
            flag.pos.y - 5,
            flag.pos.x - 5,
            flag.pos.y + 5,
            flag.pos.x + 5,
            true
        );
        for (let i in lookResults)
        {
            // noinspection JSUnfilteredForInLoop
            if (lookResults[i].structure.structureType === STRUCTURE_CONTAINER)
            {
                flag.setColor(COLOR_YELLOW, COLOR_GREEN);
                return GoalProviderHarvestToContainer.evaluate(flag);
            }
        }

        flag.setColor(COLOR_YELLOW, COLOR_YELLOW);
        return GoalProviderHarvestBasic.evaluate(flag);
    }
};

/**
 * creates a goal to simply harvest from the source with no further logic attached
 * @type {{evaluate(flag: Flag): Goal[]}}
 */
export const GoalProviderHarvestBasic : GoalProvider =
    {
        evaluate(flag: Flag) : Goal[]
        {
            const room = flag.room;
        if(!room)
        {
            console.log(Game.time + " [WARN] Removing harvest flag " + flag.name + " because the room could not be found.");
            flag.remove();
            return [];
        }

        // locate a source underneath the flag
        if (!flag.memory.target)
        {
            const targetSource:Source = room.lookForAt(LOOK_SOURCES, flag.pos)[0];

            if(targetSource)
                flag.memory.target = targetSource.id;
            else
            {
                console.log(Game.time + " [" + room.name + "][WARN] target of flag at " + flag.pos + " is not a valid source");
                return [];
            }

            const fields = room.lookForAtArea(LOOK_TERRAIN, flag.pos.y - 1, flag.pos.x - 1, flag.pos.y + 1, flag.pos.x + 1, true);
            flag.memory.value = 9 - _.countBy(fields, "terrain").wall;
        }


        return[
            {
                //single high priority harvest to make sure the source is put to use
                name: "Harvest energy",
                pos: flag.pos,
                minimumOpenCapacity: 10,
                prefersOpenCapacity: 50,
                partsRequested: [{part: WORK, preferred:4, max:4}, {part:CARRY}],
                priority: PRIORITY_HARVEST,
                behaviorName: BehaviorHarvestEnergy.name,
                targetId: flag.memory.target,
                maximumCreepCount:1,
            },
            {
                //more low priority harvesting for creeps with nothing better to do
                name: "Harvest energy",
                pos: flag.pos,
                minimumOpenCapacity: 10,
                prefersOpenCapacity: 50,
                partsRequested: [{part: WORK, preferred:4, max:4}, {part:CARRY}],
                priority: PRIORITY_HARVEST_EXTRA,
                behaviorName: BehaviorHarvestEnergy.name,
                targetId: flag.memory.target,
                maximumCreepCount:flag.memory.value,
            }];
        }
    };

/**
 * creates a goal to harvest from an energy source, put it in a nearby container, and repeat
 * @type {{evaluate(flag: Flag): Goal[]}}
 */
export const GoalProviderHarvestToContainer : GoalProvider =
    {
        evaluate(flag: Flag) : Goal[]
        {
            const room = flag.room;
            if(!room)
            {
                console.log("Removing harvest flag " + flag.name + " because the room could not be found.");
                flag.remove();
                return [];
            }

            // find relevant information
            let targetObject: Source | Mineral;

            if (flag.memory.target)
            {
                targetObject = <Source | Mineral>Game.getObjectById<Source | Mineral>(flag.memory.target);
                if(!targetObject)
                {
                    //if the target went away, remove the target from memory and move on
                    delete flag.memory.target;
                    return [];
                }
            }
            else
            {
                //check for energy sources or mineral deposits
                targetObject =
                    room.lookForAt(LOOK_SOURCES, flag.pos)[0] ||
                    room.lookForAt(LOOK_MINERALS, flag.pos)[0];

                if(!targetObject)
                {
                    console.log(Game.time + " [" + room.name + "][WARN] harvest flag at " + flag.pos + " can't find a target!");
                    return [];
                }

                flag.memory.target = targetObject.id;
            }

            if(!flag.memory.value)
            {
                const fields = room.lookForAtArea(LOOK_TERRAIN, flag.pos.y - 1, flag.pos.x - 1, flag.pos.y + 1, flag.pos.x + 1, true);

                flag.memory.value = 9 - _.countBy(fields, "terrain").wall;
            }

            // find closest container.  this is not in the above block because the closest container may change
            // for the purposes of harvesting, a storage structure can count as a container
            const closestContainer = flag.pos.findClosestByRange(roomContainersAndStorage(room));

            if(!closestContainer)
            {
                if(room.memory.logSettings.debug)
                    console.log(Game.time + " [" + room.name + "][WARN]: harvestToContainer goal has no container.");

                //if this is a source, revert to basic harvest so we dont starve on energy.  otherwise, return no goals.
                if((<Source>targetObject).energyCapacity)
                {
                    if(room.memory.logSettings.debug)
                        console.log(Game.time + " [" + room.name + "][WARN]: reverting to basic harvest goal");
                    return GoalProviderHarvestBasic.evaluate(flag);
                }
                else
                {
                    return [];
                }
            }

            //don't return a goal if the container doesn't have room to hold the spoils
            if( (closestContainer.storeCapacity - _.sum(closestContainer.store)) < 200)
                return [];

            //return goals differently based on the type of the target
            if((<Source>targetObject).energyCapacity)
            {
                if ((<Source>targetObject).energy === 0)
                    return [];

                //if the container is distant, revert to just a basic harvest so we dont waste time harvesting inefficiently and create supply problems
                if(flag.pos.getRangeTo(closestContainer) > 5)
                {
                    if(room.memory.logSettings.debug)
                        console.log(Game.time + " [" + room.name + "][WARN]: using harvestToContainer but container is distant! reverting to basic harvest goal");
                    return GoalProviderHarvestBasic.evaluate(flag);
                }

                //the target is a source that has energy
                return [
                    {
                        //single high priority harvest to make sure the source is put to use
                        name: "Harvest energy to container",
                        pos: flag.pos,
                        minimumOpenCapacity: 10,
                        prefersOpenCapacity: 50,
                        partsRequested: [{part: WORK, preferred:4, max: 10}, {part:CARRY}],
                        priority: PRIORITY_HARVEST,
                        behaviorName: BehaviorHarvestToContainer.name,
                        targetId: flag.memory.target,
                        maximumCreepCount:1,
                    },
                    {
                        //more low priority harvesting for creeps with nothing better to do
                        name: "Harvest energy to container",
                        pos: flag.pos,
                        minimumOpenCapacity: 10,
                        prefersOpenCapacity: 50,
                        partsRequested: [{part: WORK, preferred: 4, max: 10}, {part: CARRY}],
                        priority: PRIORITY_HARVEST_EXTRA,
                        behaviorName: BehaviorHarvestToContainer.name,
                        targetId: flag.memory.target,
                        maximumCreepCount: flag.memory.value,
                    }];
            }
            else if((<Mineral>(targetObject)).mineralType)
            {
                //don't harvest from exhausted deposits
                if( (<Mineral>targetObject).mineralAmount === 0)
                    return [];

                //don't harvest if the room has no storage structure to put it in
                if(!room.storage)
                    return [];

                //make sure it has an extractor
                if(!targetObject.pos.lookFor(LOOK_STRUCTURES).some(s => s.structureType === STRUCTURE_EXTRACTOR))
                {
                    if(room.memory.logSettings.debug)
                        console.log(Game.time + " [" + room.name + "] harvestToContainer flag at " + flag.pos + " is waiting for an extractor to be built.");
                    return [];
                }

                //warn if the only container is distant, but harvest anyway
                if(flag.pos.getRangeTo(closestContainer) > 5)
                    if(room.memory.logSettings.debug)
                        console.log(Game.time + " [" + room.name + "][WARN]: using harvestToContainer but container is distant!");

                //the target is a mineral that has resources and a harvester
                return [
                    {
                        //more low priority harvesting for creeps with nothing better to do
                        name: "Harvest mineral to container",
                        pos: flag.pos,
                        minimumOpenCapacity: 10,
                        prefersOpenCapacity: 50,
                        partsRequested: [{part: WORK, preferred: 4, max: 10}, {part: CARRY}],
                        priority: PRIORITY_HARVEST_MINERAL,
                        behaviorName: BehaviorHarvestToContainer.name,
                        targetId: flag.memory.target,
                        maximumCreepCount: 1,
                    }];
            }
            else
            {
                console.log(Game.time + " [" + room.name + "] [WARN]: Invalid target type! (" + JSON.stringify(targetObject) + ")");
                return [];
            }
        }
    };

/**
 * creates goals to form a raiding party at the flag
 * @type {{evaluate(flag: Flag): (any[] | ({name: string; pos: RoomPosition; partsRequested: {part: "claim"}[]; priority: GoalPriority; behaviorName; targetId: string; currentCreepCount: CreepCounts; maximumCreepCount: number} | {name: string; pos: RoomPosition; partsRequested: {part: "attack"}[]; priority: GoalPriority; behaviorName; targetId: string; currentCreepCount: CreepCounts})[])}}
 */
export const GoalProviderMusterRaidingParty : GoalProvider =
    {
        evaluate(flag:Flag)
        {
            if(!flag.room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return [];
            }

            return [
                {
                    name: "Muster for raid (claimer)",
                    pos: flag.pos,
                    partsRequested: [{part: CLAIM}],
                    priority: PRIORITY_MUSTER,
                    behaviorName: BehaviorMuster.name,
                    targetId: flag.name,
                    maximumCreepCount:1,
                    ifRequirementsUnmetSpawn:"Claimer",
                },
                {
                    name: "Muster for raid (soldier)",
                    pos: flag.pos,
                    partsRequested: [{part: ATTACK}],
                    priority: PRIORITY_MUSTER,
                    behaviorName: BehaviorMuster.name,
                    targetId: flag.name,
                    maximumCreepCount:3,
                    ifRequirementsUnmetSpawn:"Soldier",
                }
            ];
        }
    };

/**
 * creates goal to send a single claimer to the flag
 * @type {{evaluate(flag: Flag): (any[] | ({name: string; pos: RoomPosition; partsRequested: {part: "claim"}[]; priority: GoalPriority; behaviorName; targetId: string; currentCreepCount: CreepCounts; maximumCreepCount: number} | {name: string; pos: RoomPosition; partsRequested: {part: "attack"}[]; priority: GoalPriority; behaviorName; targetId: string; currentCreepCount: CreepCounts})[])}}
 */
export const GoalProviderMusterClaimer : GoalProvider =
    {
        evaluate(flag:Flag)
        {
            if(!flag.room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return [];
            }

            return [
                {
                    name: "Mustering claimer",
                    pos: flag.pos,
                    partsRequested: [{part: CLAIM}],
                    priority: PRIORITY_MUSTER,
                    behaviorName: BehaviorMuster.name,
                    targetId: flag.name,
                    maximumCreepCount:1,
                    ifRequirementsUnmetSpawn:"Claimer",
                }
            ];
        }
    };

/**
 * creates a goal to form a construction crew at the flag
 * @type {{}}
 */
export const GoalProviderMusterConstructionCrew : GoalProvider =
    {
        evaluate(flag:Flag)
        {
            if(!flag.room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return [];
            }

            return [
                {
                    name: "Muster for construction",
                    minimumEnergy:50,
                    pos: flag.pos,
                    partsRequested: [{part: CARRY}, {part: WORK}],
                    priority: PRIORITY_MUSTER,
                    behaviorName: BehaviorMuster.name,
                    targetId: flag.name,
                    maximumCreepCount:3,
                }
            ];
        }
    };

/**
 * sends any creeps that would otherwise be idle to the flag position.
 * @type {{evaluate(flag: Flag)}}
 */
export const GoalProviderIdleRoute : GoalProvider =
    {
        evaluate(flag:Flag)
        {
            return [
                {
                    name: "Idle route",
                    partsRequested:[],
                    priority:PRIORITY_IDLE,
                    behaviorName:BehaviorGoToPosition.name,
                    pos: flag.pos,
                }
            ];
        }
    };

/**
 * supplies the lab from local containers/storage.  It only supplies the lab with multiples of 15, since this is what the operation size is for running reactions
 * @type {{evaluate(flag: Flag): any[]}}
 */
export const GoalProviderLabSupplyLocal : GoalProvider =
    {
        evaluate(flag:Flag)
        {
            const result: Goal[] = [];
            const targetLab: StructureLab = <StructureLab>_.find(flag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);

            if(!targetLab)
            {
                if(!flag.pos.lookFor(LOOK_CONSTRUCTION_SITES).some(cs => cs.structureType === STRUCTURE_LAB)) //dont bother warning if a lab is already under construction
                    console.log(Game.time + " [" + flag.pos.roomName + "][WARN] a LAB_SUPPLY_LOCAL flag has no lab!");
                return [];
            }

            if(!flag.memory.target)
                flag.memory.target = null;

            if(flag.memory.target === null)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "][WARN] a LAB_SUPPLY_LOCAL flag is unconfigured!");
            }
            else
            {
                if( (targetLab.mineralCapacity - targetLab.mineralAmount) >= 500)
                    if(targetLab.room.storage && (targetLab.room.storage.store as any)[flag.memory.target])
                        result.push(
                            {
                                name: "Supply lab with mineral",
                                pos: targetLab.pos,
                                partsRequested: [{part: CARRY}],
                                priority: PRIORITY_SUPPLY_LAB,
                                behaviorName: BehaviorSupplyLabWithMineral.name,
                                targetId: flag.memory.target, //this particular behavior needs two bits of info, so it figures out the ID for itself and actually uses this field for the mineral
                                maximumCreepCount: 1,
                            }
                        );
            }

            return result;
        }
    };

/**
 * provides goals remove minerals from the lab and put them in storage
 * @type {{}}
 */
export const GoalProviderCollectFromLab: GoalProvider =
    {
        evaluate(flag: Flag)
        {
            if(!flag.memory || !flag.memory.target)
                return [];

            const targetLab: StructureLab = <StructureLab>Game.getObjectById(flag.memory.target);

            if(!targetLab)
            {
                if(!flag.pos.lookFor(LOOK_CONSTRUCTION_SITES).some(cs => cs.structureType === STRUCTURE_LAB)) //dont bother warning if a lab is already under construction
                    console.log(Game.time + " [" + flag.pos.roomName + "] GoalProviderCollectFromLab can't find a lab! (flag.memory.target)");
                return [];
            }

            if(targetLab.mineralAmount >= 200)
            {
                return [{
                    name: "Collect minerals from lab",
                    behaviorName:BehaviorCollectFromLab.name,
                    partsRequested:[{part:CARRY, min:1, preferred:(Math.ceil(targetLab.mineralAmount / 50))}],
                    priority:((targetLab.mineralCapacity - targetLab.mineralAmount) <= 200) ? PRIORITY_COLLECT_URGENT : PRIORITY_COLLECT,
                    pos:targetLab.pos,
                    targetId: targetLab.id,
                    maximumCreepCount:1,
                    minimumOpenCapacity:50,
                    prefersOpenCapacity:targetLab.mineralAmount,
                }];
            }

            return [];
        }
    };

/**
 * empties out the target lab
 * @type {{}}
 */
export const GoalProviderLabCleanup: GoalProvider =
    {
        evaluate(flag: Flag)
        {
            const targetLab: StructureLab = <StructureLab>_.find(flag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);

            if(!targetLab)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "] GoalProviderLabCleanup can't find a lab! (flag.memory.target)");
                return [];
            }

            if(targetLab.mineralAmount > 0)
            {
                return [{
                    name: "Collect minerals from lab",
                    behaviorName:BehaviorCollectFromLab.name,
                    partsRequested:[{part:CARRY}],
                    priority: PRIORITY_CLEANUP_RESOURCES,
                    pos:targetLab.pos,
                    targetId: targetLab.id,
                    maximumCreepCount:1,
                    minimumOpenCapacity:50,
                    prefersOpenCapacity:targetLab.mineralAmount,
                }];
            }
            else
            {
                console.log("lab cleanup complete in " + flag.pos.roomName);
                flag.remove();
            }

            return [];
        }
    };
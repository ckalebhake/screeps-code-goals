//this file is for code that should be run by a flag every tick
import {BehaviorGoToPosition, BehaviorGoToRoom, BehaviorTransportEnergy, BehaviorTransportMinerals} from "./Behaviors";
import {PRIORITY_SUPPLY_LAB} from "./goalPriorities";
import {countCreeps, creepCountsLessThan} from "./utils/lib.creep";
import {PRIORITY_COLLECT_URGENT, PRIORITY_MUSTER} from "./goalPriorities";

/**
 * removes all goals from creeps in the room so they reconsider what they are doing
 * @type {{run: (flag: Flag) => void}}
 */
export const FlagOrderDebugReassignGoals: FlagOrder =
    {
        run: function (flag: Flag): void
        {
            if (!flag.room)
            {
                flag.remove();
                return;
            }

            flag.room.find(FIND_MY_CREEPS).forEach(c => delete c.memory.goal);
            flag.remove();
            console.log(Game.time + " [" + flag.room.name + "] debug flag executed: all creep goals removed");
        }
    };

/**
 * purges the memory of all creeps and flags in the room
 * @type {{run: (flag: Flag) => void}}
 */
export const FlagOrderDebugPurgeMemory: FlagOrder =
    {
        run: function (flag: Flag): void
        {
            if (!flag.room)
            {
                flag.remove();
                return;
            }

            flag.room.find(FIND_MY_CREEPS).forEach(c =>
            {
                delete c.memory.goal;
                delete c.memory.behaviorMemory;
            });
            flag.room.find(FIND_FLAGS).forEach(f =>
            {
                delete f.memory;
            });

            flag.remove();
            console.log(Game.time + " [" + flag.room.name + "] debug flag executed: all creep and flag memory purged");
        }
    };

/**
 * common behavior to all muster flags
 * @param {Flag} flag
 * @param count
 */
function muster(flag: Flag, count: number)
{
    if (!flag.memory.flagCreeps)
    {
        flag.memory.flagCreeps = [];
    }
    else
    {
        if (flag.memory.flagCreeps.length >= count)
        {
            flag.memory.flagCreeps.forEach((creepName: string) =>
            {
                const creep = Game.creeps[creepName];
                console.log("muster flag assigning new goal to " + creepName);
                const closestExit = flag.pos.findClosestByPath(FIND_EXIT);
                if(closestExit)
                {
                    creep.memory.goal =
                        {
                            name: "Go to position",
                            pos: closestExit,
                            partsRequested: [],
                            priority: PRIORITY_MUSTER,
                            behaviorName: BehaviorGoToPosition.name,
                        };
                }
                else
                {
                    console.log("muster flag can't send creeps to another room: no exits from room " + creep.pos.roomName);
                }
            });
            flag.remove();
        }
    }
}

/**
 * waits for a full compliment of creeps to have arrived, then sends them through the nearest exit
 * @type {{run: (flag: Flag) => void}}
 */
export const FlagOrderMusterRaidingParty: FlagOrder =
    {
        run: function (flag: Flag): void
        {
            if (!flag.memory.flagCreeps)
                flag.memory.flagCreeps = [];
            else
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter((c: string) => Game.creeps[c]); //filter out creeps that no longer exist

            const room = flag.room;
            if (!room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return;
            }

            muster(flag, 4);
        }
    };

/**
 * gathers up a construction crew at the flag and then sends them through the nearest exit
 * @type {{run: (flag: Flag) => void}}
 */
export const FlagOrderMusterConstructionCrew: FlagOrder =
    {
        run: function (flag: Flag): void
        {
            if (!flag.memory.flagCreeps)
                flag.memory.flagCreeps = [];
            else
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter((c: string) => Game.creeps[c]); //filter out creeps that no longer exist

            const room = flag.room;
            if (!room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return;
            }

            muster(flag, 3);
        }
    };

/**
 * musters a single raider at the flag and then sends it through the nearest exit
 * @type {{run: (flag: Flag) => void}}
 */
export const FlagOrderMusterClaimer: FlagOrder =
    {
        run: function (flag: Flag): void
        {
            if (!flag.memory.flagCreeps)
                flag.memory.flagCreeps = [];
            else
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter((c: string) => Game.creeps[c]); //filter out creeps that no longer exist

            const room = flag.room;
            if (!room)
            {
                console.log("[WARN] Removing muster flag in unseen room!");
                flag.remove();
                return;
            }

            muster(flag, 1);
        }
    };

/**
 * waits for the room to be under our control, then starts construction of a spawn at this position
 * also requests remote spawns throughout, so that there are creeps around to do the necessary work
 */
export const FlagOrderBuildSpawnHere: FlagOrder =
    {
        run(flag: Flag)
        {
            //make sure we have required memory objects
            if (!flag.memory.flagCreeps)
                flag.memory.flagCreeps = [];
            else
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter((c: string) => Game.creeps[c]); //filter out creeps that no longer exist

            //decide what kind of help we want from nearby rooms.
            flag.memory.requestingCreepType = "Soldier";

            //if we have vision on the room, we can use that to order construction and possibly pick a better unit type to send over
            if (flag.room)
            {
                if (!flag.room.controller)
                {
                    console.log(Game.time + " [" + flag.room.name + "] Can't build a spawn in this room because there is no controller!  removing flag.");
                    flag.remove();
                    return;
                }

                if ((flag.room.find(FIND_HOSTILE_CREEPS).length > 0) || (flag.room.find(FIND_HOSTILE_STRUCTURES).length > 1)) //the controller counts as a hostile structure, so we only need soldiers if there are at least 2 hostile structures
                    flag.memory.requestingCreepType = "Soldier";
                else if (!flag.room.controller.my)
                    flag.memory.requestingCreepType = "Claimer";
                else
                    flag.memory.requestingCreepType = "Builder";

                if (flag.room.controller)
                {
                    if (flag.room.controller.my)
                    {
                        if (flag.pos.lookFor(LOOK_STRUCTURES).some(s => s.structureType === STRUCTURE_SPAWN))
                            flag.remove();
                        else if (!flag.pos.lookFor(LOOK_CONSTRUCTION_SITES).some(s => s.structureType === STRUCTURE_SPAWN))
                            flag.room.createConstructionSite(flag.pos, STRUCTURE_SPAWN);
                    }
                }

                //skip asking for assistance if the room is already crowded
                //if(flag.room.find(FIND_MY_CREEPS).length >= 8)
                const desiredCreepCountsPerDesiredUnitType: { [index: string]: CreepCounts } =
                    {
                        Soldier: {creeps: 6, parts: [{count: 12, part: ATTACK}]},
                        Claimer: {creeps: 1, parts: [{count: 1, part: CLAIM}]},
                        Builder: {creeps: 6, parts: [{count: 12, part: WORK}]},
                    };
                if (!creepCountsLessThan(countCreeps(flag.room), desiredCreepCountsPerDesiredUnitType[flag.memory.requestingCreepType]))
                    return;
            }

            //compile a list of rooms within range to assist this one of at least level 3
            const spawnRequest: RemoteSpawnRequest = {requestingFlagName: flag.name};

            for (const roomName in Game.rooms)
            {
                const room = Game.rooms[roomName];

                if (room.controller &&
                    room.controller.my &&
                    (room.controller.level >= 3) &&
                    ((<any[]>Game.map.findRoute(roomName, flag.pos.roomName)).length <= 3))
                {
                    //find the spawn
                    const assistingSpawn = room.find(FIND_MY_SPAWNS)[0];

                    if (!assistingSpawn)
                        continue;

                    if (!assistingSpawn.memory.remoteSpawnRequests)
                        assistingSpawn.memory.remoteSpawnRequests = [];

                    if (!assistingSpawn.memory.remoteSpawnRequests.some(rsr => (rsr.requestingFlagName === spawnRequest.requestingFlagName)))
                        assistingSpawn.memory.remoteSpawnRequests.push(spawnRequest);
                }
            }
        }
    };

/**
 * purges room settings of the room containing this flag
 * @type {{run(flag: Flag): void}}
 */
export const FlagOrderDebugPurgeRoomSettings: FlagOrder =
    {
        run(flag: Flag)
        {
            const roomName = flag.pos.roomName;

            delete Memory.rooms[roomName].constructionSettings;
            delete Memory.rooms[roomName].spawnSettings;
            delete Memory.rooms[roomName].logSettings;
            delete Memory.rooms[roomName].repairSettings;

            console.log(Game.time + " [" + roomName + "] debug flag executed: all room settings purged");
            flag.remove();
        }
    };

/**
 * purges room settings of all rooms
 * @type {{run(flag: Flag): void}}
 */
export const FlagOrderDebugPurgeAllRoomSettings: FlagOrder =
    {
        run(flag: Flag)
        {
            for (const roomName in Memory.rooms)
            {
                delete Memory.rooms[roomName].constructionSettings;
                delete Memory.rooms[roomName].spawnSettings;
                delete Memory.rooms[roomName].logSettings;
                delete Memory.rooms[roomName].repairSettings;
            }

            console.log(Game.time + " debug flag executed: all room settings purged from ALL rooms.");
            flag.remove();
        }
    };

/**
 * responsible for supplying a lab with minerals from other rooms
 * @type {{run(flag: Flag)}}
 */
export const FlagOrderSupplyLabRemote: FlagOrder =
    {
        run(flag: Flag)
        {
            //ensure we have a target mineral
            if (!flag.memory.target)
                flag.memory.target = null;

            if (flag.memory.target === null)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "][WARN] a LAB_SUPPLY_REMOTE flag is unconfigured!");
                return;
            }

            //ensure we have a target lab
            const targetLab: StructureLab = <StructureLab>_.find(flag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);
            if (!targetLab)
            {
                if(!flag.pos.lookFor(LOOK_CONSTRUCTION_SITES).some(cs => cs.structureType === STRUCTURE_LAB)) //dont bother warning if a lab is already under construction
                    console.log(Game.time + " [" + flag.pos.roomName + "] SUPPLY_LAB_REMOTE flag cant find a lab to supply");
                return;
            }

            //filter dead creeps out of memory
            if (flag.memory.flagCreeps && flag.memory.flagCreeps.length >= 1)
            {
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter(creepName => Game.creeps[creepName]);
            }
            //only act if the lab is almost empty and has nothing currently supplying it
            if (targetLab.mineralAmount <= 100 && (!flag.memory.flagCreeps || flag.memory.flagCreeps.length === 0))
            {
                //before looking elsewhere, see if the local storage has what we need
                //flags of this type also have the goal provider from LAB_SUPPLY_LOCAL, so if this is the case the order can just sit idle
                if (flag.room && flag.room.storage && (flag.room.storage as any)[flag.memory.target])
                    return;

                flag.memory.requestingCreepType = "Carrier";
                flag.memory.requestingCreepMemory = {
                    goal:
                        {
                            name: "Transport minerals",
                            partsRequested: [],
                            priority: PRIORITY_SUPPLY_LAB,
                            behaviorName: BehaviorTransportMinerals.name,
                            pos: targetLab.pos,
                            targetId: flag.name,
                        }
                };

                //send a request to all rooms within range of this one that have the requested mineral
                const spawnRequest: RemoteSpawnRequest = {requestingFlagName: flag.name};

                for (const roomName in Game.rooms)
                {
                    const room = Game.rooms[roomName];

                    if (room.controller &&
                        room.controller.my &&
                        (room.controller.level >= 6) &&
                        (room.storage && (room.storage.store as any)[flag.memory.target] && (room.storage.store as any)[flag.memory.target] >= 1000) &&
                        ((<any[]>Game.map.findRoute(roomName, flag.pos.roomName)).length <= 3))
                    {
                        //find the spawn
                        const assistingSpawn = room.find(FIND_MY_SPAWNS)[0];

                        if (!assistingSpawn)
                            continue;

                        if (!assistingSpawn.memory.remoteSpawnRequests)
                            assistingSpawn.memory.remoteSpawnRequests = [];

                        if (!assistingSpawn.memory.remoteSpawnRequests.some(rsr => (rsr.requestingFlagName === spawnRequest.requestingFlagName)))
                            assistingSpawn.memory.remoteSpawnRequests.push(spawnRequest);
                    }
                }
            }
            else
            {
                //we either already have a carrier or the lab still has minerals in it, so we dont need any more yet.
                delete flag.memory.requestingCreepType;
            }
        }
    };

/**
 * this flag causes the lab underneath it to run a reaction with nearby labs.
 * @type {{}}
 */
    //TODO: allow this to take a target mineral
export const FlagOrderLabProduce: FlagOrder =
        {
            run(flag: Flag)
            {
                let targetLab: StructureLab | null = null;
                if(flag.memory.target)
                {
                    targetLab = (Game.getObjectById(flag.memory.target) || null);
                    if(!targetLab)
                        delete flag.memory.target;
                }

                if (!flag.memory.target)
                {
                    targetLab = <StructureLab>_.find(flag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);
                    flag.memory.target = targetLab.id;
                }

                if (!targetLab)
                {
                    if(!flag.pos.lookFor(LOOK_CONSTRUCTION_SITES).some(cs => cs.structureType === STRUCTURE_LAB)) //dont bother warning if a lab is already under construction
                        console.log(Game.time + " [" + flag.pos.roomName + "] LAB_PRODUCE flag cant find a lab to make produce stuff");
                    return;
                }

                if (!flag.room)
                    return;

                const labsNearTargetLab: StructureLab[] = <StructureLab[]>flag.room.find(FIND_MY_STRUCTURES, {
                    filter: structure =>
                    {
                        return structure.structureType === STRUCTURE_LAB &&   //structure is a lab
                            structure.id !== targetLab!.id &&              //but not the one trying to run the reaction
                            structure.pos.getRangeTo(targetLab!.pos) <= 2  //and is within range
                    }
                });

                if (labsNearTargetLab.length !== 2)
                {
                    console.log(Game.time + " [" + flag.pos.roomName + "] LAB_PRODUCE flag has too few or too many nearby labs to choose from");
                    return;
                }

                targetLab.runReaction(labsNearTargetLab[0], labsNearTargetLab[1]);
            }
        };

/**
 * resets logging in all rooms to their default values
 * @type {{run(flag: Flag): void}}
 */
export const FlagOrderDebugResetLogging: FlagOrder =
    {
        run(flag: Flag)
        {
            for (const roomName in Memory.rooms)
                Memory.rooms[roomName].logSettings = Memory.globalSettings.defaultLogSettings;

            console.log(Game.time + " debug flag executed: log settings reset.");
            flag.remove();
        }
    };

/**
 * resets spawn count adjustments to 0 in all rooms
 * @type {{run(flag: Flag): void}}
 */
export const FlagOrderDebugResetSpawnAdjustments: FlagOrder =
    {
        run(flag: Flag)
        {
            for (const roomName in Memory.rooms)
                Memory.rooms[roomName].spawnCountAdjustment = 0;

            console.log(Game.time + " debug flag executed: spawn count adjustments reset.");
            flag.remove();
        }
    };

/**
 * harvests the room the flag is in using creeps from the room set as the target.  Also maintains a carrier to haul said resources back
 * @type {{run(flag: Flag)}}
 */
export const FlagOrderHarvestRemote: FlagOrder =
    {
        run(flag: Flag)
        {
            if(flag.room && (flag.room.find(FIND_MY_SPAWNS).length > 0 || ((flag.room.controller && flag.room.controller.level > 0) && flag.room.find(FIND_HOSTILE_SPAWNS).length > 0)))
            {
                console.log(Game.time + " [" + flag.room.name + "] removing HarvestRemote flag because the room now has an owner");
                flag.remove();
                return;
            }

            //filter dead creeps out of memory
            if (flag.memory.flagCreeps && flag.memory.flagCreeps.length >= 1)
            {
                flag.memory.flagCreeps = flag.memory.flagCreeps.filter(creepName => Game.creeps[creepName]);
            }

            //track down needed information
            if (!flag.memory.target)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "] HarvestRemote flag unconfigured!");
                flag.memory.target = null;
                return;
            }

            const currentRoom = Game.rooms[flag.pos.roomName];
            const targetRoom = Game.rooms[flag.memory.target];

            if (!targetRoom)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "] HarvestRemote flag can't find target room " + flag.memory.target);
                return;
            }

            const targetRoomSpawn = targetRoom.find(FIND_MY_SPAWNS)[0];
            const carrier = _.find((flag.memory.flagCreeps || []), creepName => Memory.creeps[creepName].specialized);

            if (!targetRoomSpawn)
            {
                console.log(Game.time + " [" + flag.pos.roomName + "] HarvestRemote flag can't request help from target room " + flag.memory.target + " because it has no spawn");
                return;
            }

            //decide what we want, if anything, and request it
            let desiredUnitType = null;

            if (currentRoom && currentRoom.find(FIND_HOSTILE_CREEPS).length > 0)
            {
                desiredUnitType = "Soldier";
                flag.memory.requestingCreepMemory =
                    {
                        goal:
                            {
                                targetId: flag.pos.roomName,
                                priority: PRIORITY_MUSTER,
                                name: "Move to assist room",
                                behaviorName: BehaviorGoToRoom.name,
                                partsRequested: [],
                                pos: flag.pos,
                            }
                    };
            }
            else if (!currentRoom || countCreeps(currentRoom, true).creeps < currentRoom.find(FIND_SOURCES).length)
            {
                desiredUnitType = "Builder";
                flag.memory.requestingCreepMemory =
                    {
                        goal:
                            {
                                targetId: flag.pos.roomName,
                                priority: PRIORITY_MUSTER,
                                name: "Move to assist room",
                                behaviorName: BehaviorGoToRoom.name,
                                partsRequested: [],
                                pos: flag.pos,
                            }
                    };
            }
            else if ((flag.room && flag.room.find(FIND_MY_CONSTRUCTION_SITES).length === 0) && !carrier)
            {
                desiredUnitType = "Carrier";
                flag.memory.requestingCreepMemory =
                    {
                        goal:
                            {
                                targetId: flag.pos.roomName,
                                priority: PRIORITY_COLLECT_URGENT,
                                name: "transport energy",
                                behaviorName: BehaviorTransportEnergy.name,
                                partsRequested: [],
                                pos: flag.pos,
                            },
                        specialized:true
                    };
            }

            if (!desiredUnitType)
            {
                delete flag.memory.requestingCreepType;
            }
            else
            {
                flag.memory.requestingCreepType = desiredUnitType;
                if (!targetRoomSpawn.memory.remoteSpawnRequests)
                    targetRoomSpawn.memory.remoteSpawnRequests = [];
                if (!_.some(targetRoomSpawn.memory.remoteSpawnRequests, rsr => rsr.requestingFlagName === flag.name))
                    targetRoomSpawn.memory.remoteSpawnRequests.push({requestingFlagName: flag.name});
            }

            //make sure automation picks up this room for harvest flags and container construction
            if (currentRoom)
                currentRoom.memory.remoteHarvestingInRoom = true;
        }
    };
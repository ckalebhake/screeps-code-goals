import {roomContainersAndStorage, roomSpawnsAndExtensions} from "./utils/lib.room";

//TODO: automate expansion
export function autoConstruct(room: Room)
{
    //builds containers near energy sources and extractors
    if ( (room.memory.constructionSettings.containersBySources || room.memory.constructionSettings.containersByExtractors) &&
         ((room.controller && room.controller.my) || room.memory.remoteHarvestingInRoom))
    {
        //look for container/storage structures near the source
        const structures: (StructureContainer | StructureStorage | ConstructionSite)[] = [];
        structures.push(...roomContainersAndStorage(room));
        //also include construction sites for those structures so we dont build extra containers
        structures.push(...room.find(FIND_MY_CONSTRUCTION_SITES, {
            filter: (cs) =>
                cs.structureType === STRUCTURE_CONTAINER || cs.structureType === STRUCTURE_STORAGE
        }));

        //find all energy sources and extractors that dont have one of those within 10 squares
        const roomSourcesWithoutContainers: Source[] = room.find(FIND_SOURCES, {
            filter: function (source)
            {
                const structuresNearSource = source.pos.findInRange(structures, 5);
                return structuresNearSource.length === 0;
            }
        });

        const roomExtractorsWithoutContainers: AnyOwnedStructure[] = room.find(FIND_MY_STRUCTURES, {
            filter: function (s)
            {
                if (s.structureType !== STRUCTURE_EXTRACTOR)
                    return false;

                const structuresNearExtractor = s.pos.findInRange(structures, 5);
                return structuresNearExtractor.length === 0;
            }
        });

        const thingsNeedingContainers: (Source | AnyOwnedStructure)[] = [];
        thingsNeedingContainers.push(...roomSourcesWithoutContainers);
        thingsNeedingContainers.push(...roomExtractorsWithoutContainers);

        //construct containers near those sources
        outerLoop: for (let i = 0; i < thingsNeedingContainers.length; i++)
        {
            const thingPos = thingsNeedingContainers[i].pos;

            //first attempt to build it in the direction of the center of the room
            if (room.createConstructionSite(thingPos.x + ((Math.sign(25 - thingPos.x) * 2)),
                thingPos.y + ((Math.sign(25 - thingPos.y) * 2)),
                STRUCTURE_CONTAINER) === OK)
                continue;

            //that didnt work, so try it again a bit further away
            if (room.createConstructionSite(thingPos.x + ((Math.sign(25 - thingPos.x) * 4)),
                thingPos.y + ((Math.sign(25 - thingPos.y) * 4)),
                STRUCTURE_CONTAINER) === OK)
                continue;

            //that didnt work either, so just try some random spots 2-5 tiles away from the thing
            const offsets = [{x: -2, y: -2}, {x: -1, y: -2}, {x: 0, y: -2}, {x: 1, y: -2}, {x: 2, y: -2},
                {x: -2, y: -1}, {x: -1, y: -1}, {x: 0, y: -1}, {x: 1, y: -1}, {x: 2, y: -1},
                {x: -2, y: 0}, {x: -1, y: 0}, {x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0},
                {x: -2, y: 1}, {x: -1, y: 1}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 2, y: 1},
                {x: -2, y: 2}, {x: -1, y: 2}, {x: 0, y: 2}, {x: 1, y: 2}, {x: 2, y: 2}];

            //try every spot two away from the thing
            for (let j = 0; j < offsets.length; j++)
            {
                if (room.createConstructionSite(thingPos.x + offsets[i].x,
                    thingPos.y + offsets[i].y,
                    STRUCTURE_CONTAINER) === OK)
                    continue outerLoop;
            }
        }
    }

    const mainSpawn = room.find(FIND_MY_SPAWNS)[0];

    if (!mainSpawn)
        return; //no spawn point in this room, skip the rest.

    const spawnPos = mainSpawn.pos;

    //make sure we have somewhere to store construction state
    if (!room.memory.constructionMemory)
        room.memory.constructionMemory = {};

    if ((!room.controller) || (room.controller.level === 0))
        return; //skip unowned rooms

    //spawn extensions
    if (room.memory.constructionSettings.spawnExtensions && room.controller.level >= 2)
    {
        //setup variables
        let buildOffset = room.memory.constructionMemory.nextSpawnExtensionOffset || {x: -1, y: -1};
        let buildDirection = room.memory.constructionMemory.SpawnExtensionBuildDirection || RIGHT;

        //attempt to build an extension at that position
        switch (room.createConstructionSite(spawnPos.x + buildOffset.x, spawnPos.y + buildOffset.y, STRUCTURE_EXTENSION))
        {
            case OK:
            case ERR_INVALID_TARGET:
                //in these cases we either successfully built an extension or can never build one here
                //and so we should move on to the next pos the next time this is called
                switch (buildDirection)
                {
                    case RIGHT:
                        buildOffset.x += 2;
                        if (buildOffset.x === (-buildOffset.y))
                            buildDirection = BOTTOM;
                        break;
                    case BOTTOM:
                        buildOffset.y += 2;
                        if (buildOffset.y === buildOffset.x)
                            buildDirection = LEFT;
                        break;
                    case LEFT:
                        buildOffset.x -= 2;
                        if (buildOffset.x === (-buildOffset.y))
                            buildDirection = TOP;
                        break;
                    case TOP:
                        buildOffset.y -= 2;
                        if (buildOffset.y === buildOffset.x)
                        {
                            buildDirection = RIGHT;
                            buildOffset.x--;
                            buildOffset.y--;
                        }
                        break;
                }

                //update memory with the new data
                room.memory.constructionMemory.nextSpawnExtensionOffset = buildOffset;
                room.memory.constructionMemory.SpawnExtensionBuildDirection = buildDirection;
                break;
        }
    }

    //roads adjacent to spawns and spawn extensions, if those spaces are swamp.
    if (room.memory.constructionSettings.roadsBySpawnsAndExtensions)
    {
        const structures = roomSpawnsAndExtensions(room);
        for (let i = 0; i < structures.length; i++)
        {
            const structurePosition = structures[i].pos;
            if (room.lookForAt(LOOK_TERRAIN, structurePosition.x - 1, structurePosition.y)[0] === "swamp")
                room.createConstructionSite(structurePosition.x - 1, structurePosition.y, STRUCTURE_ROAD);
            if (room.lookForAt(LOOK_TERRAIN, structurePosition.x + 1, structurePosition.y)[0] === "swamp")
                room.createConstructionSite(structurePosition.x + 1, structurePosition.y, STRUCTURE_ROAD);
            if (room.lookForAt(LOOK_TERRAIN, structurePosition.x, structurePosition.y - 1)[0] === "swamp")
                room.createConstructionSite(structurePosition.x, structurePosition.y - 1, STRUCTURE_ROAD);
            if (room.lookForAt(LOOK_TERRAIN, structurePosition.x, structurePosition.y + 1)[0] === "swamp")
                room.createConstructionSite(structurePosition.x, structurePosition.y + 1, STRUCTURE_ROAD);
        }
    }

    //roads adjacent to towers
    if (room.memory.constructionSettings.roadsBySpawnsAndExtensions)
    {
        const structures = room.find(FIND_MY_STRUCTURES, {filter: s => s.structureType === STRUCTURE_TOWER});
        for (let i = 0; i < structures.length; i++)
        {
            const structurePosition = structures[i].pos;
            room.createConstructionSite(structurePosition.x - 1, structurePosition.y, STRUCTURE_ROAD);
            room.createConstructionSite(structurePosition.x + 1, structurePosition.y, STRUCTURE_ROAD);
            room.createConstructionSite(structurePosition.x, structurePosition.y - 1, STRUCTURE_ROAD);
            room.createConstructionSite(structurePosition.x, structurePosition.y + 1, STRUCTURE_ROAD);
        }
    }

    //builds towers near the center of the room
    if (room.memory.constructionSettings.towers)
    {
        //if we can build another tower
        const towerCount = room.find(FIND_MY_STRUCTURES, {filter: s => s.structureType === STRUCTURE_TOWER}).length +
            room.find(FIND_MY_CONSTRUCTION_SITES, {filter: s => s.structureType === STRUCTURE_TOWER}).length;

        if (towerCount < CONTROLLER_STRUCTURES[STRUCTURE_TOWER][room.controller.level])
        {
            const roomCenter = {x: 24, y: 24};
            buildLoop : for (let searchDist = 0; searchDist < 10; searchDist++)
            {
                for (let y = roomCenter.y - searchDist; y <= roomCenter.y + searchDist; y++)
                {
                    for (let x = roomCenter.x - searchDist; x <= roomCenter.x + searchDist; x++)
                    {
                        const buildResult = room.createConstructionSite(x, y, STRUCTURE_TOWER);
                        switch (buildResult)
                        {
                            case OK:
                            case ERR_FULL:
                                break buildLoop; //either we made a construction site, or we never will
                            case ERR_INVALID_TARGET:
                                continue; //a tower cant be built there, move on
                            default:
                                console.log("unhandled error building tower! (" + buildResult + ")");
                                break buildLoop;
                        }
                    }
                }
            }
        }
    }

    //builds storage near the center of the room
    if (room.memory.constructionSettings.storage)
    {
        //if we can build another tower
        if (!(room.storage) && room.controller.level >= 4)
        {
            const roomCenter = {x: 24, y: 24};
            buildLoop : for (let searchDist = 0; searchDist < 10; searchDist++)
            {
                for (let y = roomCenter.y - searchDist; y <= roomCenter.y + searchDist; y++)
                {
                    for (let x = roomCenter.x - searchDist; x <= roomCenter.x + searchDist; x++)
                    {
                        const buildResult = room.createConstructionSite(x, y, STRUCTURE_STORAGE);
                        switch (buildResult)
                        {
                            case OK:
                            case ERR_FULL:
                                break buildLoop; //either we made a construction site, or we never will
                            case ERR_INVALID_TARGET:
                                continue; //a storage cant be built there, move on
                            default:
                                console.log("unhandled error building storage! (" + buildResult + ")");
                                break buildLoop;
                        }
                    }
                }
            }
        }
    }

    //builds extractors on minerals
    if (room.memory.constructionSettings.extractors)
    {
        const minerals = room.find(FIND_MINERALS);
        for (let i = 0; i < minerals.length; i++)
            room.createConstructionSite(minerals[i], STRUCTURE_EXTRACTOR);
    }
}

/**
 * automatically updates room settings based on the state of those rooms
 * @param room
 */
export function autoRoomSettings(room: Room)
{
    const roomLevel = (room.controller || {level: 0}).level;

    //update settings
    const mdh = [256, 1280, 6400, 32000, 160000, 800000, 4000000, 20000000, 100000000];
    room.memory.repairSettings =
        {
            maxDefenseHealth: mdh[roomLevel],
            wallRepairThreshold: 0
        };

    const sourceCount = room.find(FIND_SOURCES).length;
    const ss: CreepCounts[] =
        [
            {creeps: 0, parts: []},
            {creeps: 5 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 3 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 3 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 4 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 5 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 6 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 7 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
            {creeps: 8 + (2 * sourceCount) + (room.memory.spawnCountAdjustment || 0), parts: [{part: WORK, count: 10}]},
        ];
    room.memory.spawnSettings = {targetCreepCounts: ss[roomLevel]};

    if(!room.memory.constructionSettings || !room.memory.constructionSettings.forceTheseSettings)
    {
        room.memory.constructionSettings =
            {
                forceTheseSettings: false,
                containersByExtractors: true,
                containersBySources: true,
                extractors: true,
                roadsBySpawnsAndExtensions: true,
                roadsByTowers: true,
                spawnExtensions: true,
                towers: true,
                storage: true,
            }
    }
}

/**
 * automatically places flags on sources and minerals that need them
 */
export function autoHarvestFlags(room: Room)
{
    if(room && room.controller && (room.controller.my || room.memory.remoteHarvestingInRoom))
    {
        //sources
        room.find(FIND_SOURCES).forEach(source =>
        {
            const sourceFlags = source.pos.lookFor(LOOK_FLAGS);

            if (sourceFlags.length === 0)
            {
                //if there is no flag, put a harvestToContainer flag on it
                source.pos.createFlag(Math.random().toString(36).slice(2), COLOR_YELLOW, COLOR_GREEN);
            }
        });

        //minerals
        room.find(FIND_MINERALS).forEach(mineral =>
        {
            const mineralFlags = mineral.pos.lookFor(LOOK_FLAGS);

            if (mineralFlags.length === 0)
            {
                //if there is no flag but there is an extractor, put a harvestToContainer flag on it
                if (mineral.pos.lookFor(LOOK_STRUCTURES).some(s => s.structureType === STRUCTURE_EXTRACTOR))
                    mineral.pos.createFlag(Math.random().toString(36).slice(2), COLOR_YELLOW, COLOR_GREEN);
            }
        })
    }
}

/**
 * automatically adjusts spawn counts based on room statistics
 * @param {Room} room
 */
export function autoAdjustSpawnCounts(room: Room)
{
    if(!Memory.globalSettings.autoRoomSettings)
    {
        console.log("[WARN] autoAdjustSpawnCounts only applies if autoRoomSettings is turned on!");
        return;
    }

    //adjust spawn counts based on how well things have been going with the current settings
    if ((Memory.globalSettings.statisticsTrackingHistoryLength - (Game.time - (room.memory.timeOfLastCreepCountAnalysis || 0))) <= 0)
    {
        //only run in our own room that has a spawn and has a complete statistics history
        if (room.controller && room.controller.my && room.find(FIND_MY_SPAWNS).length > 0 &&
            room.memory.statistics.upgradesTicks.length === Memory.globalSettings.statisticsTrackingHistoryLength)
        {
            if (!room.memory.spawnCountAdjustment)
                room.memory.spawnCountAdjustment = 0;

            let dropSpawnCount = false;
            let raiseSpawnCount = false;

            if (room.memory.statistics.roundedAverages.idle === 0)
                raiseSpawnCount = true;

            if (room.memory.statistics.roundedAverages.idle >= 2)
                dropSpawnCount = true;

            if (dropSpawnCount)
                room.memory.spawnCountAdjustment!--;
            else if (raiseSpawnCount)
                room.memory.spawnCountAdjustment!++;

            room.memory.timeOfLastCreepCountAnalysis = Game.time;
        }
    }
}

/**
 * gradually reduces the price of old active sell orders on the market
 * also cancels old inactive sell orders
 */
export function handleOldSellOrders()
{
    const oldOrders = _.filter(Game.market.orders, order => (Game.time - order.created) >= Memory.globalSettings.oldOrderThreshold);

    if(oldOrders.length === 0)
        return;

    console.log("counted " + oldOrders.length + " old orders created over " + Memory.globalSettings.oldOrderThreshold + " ticks ago.");

    //cancel inactive orders
    oldOrders.filter(order => !order.active).forEach(order => Game.market.cancelOrder(order.id));

    //make sure storage exists for the adjustment history
    if(!Memory.globalAutomationMemory) Memory.globalAutomationMemory = {};
    if(!Memory.globalAutomationMemory.handleOldOrders) Memory.globalAutomationMemory.handleOldOrders = [];

    //adjust old sell orders
    oldOrders.forEach(oldOrder =>
        {
            const lastAdjustment = _.find(Memory.globalAutomationMemory!.handleOldOrders!, adjustment => adjustment.orderId === oldOrder.id);

            //skip orders we have already adjusted within the threshold
            if(lastAdjustment && (Game.time - lastAdjustment.lastAdjustedTime) < Memory.globalSettings.oldOrderThreshold)
                return;

            //if we have already adjusted this order once before, remove the record of it
            if(lastAdjustment)
                _.remove(Memory.globalAutomationMemory!.handleOldOrders!, lastAdjustment);

            //adjust the order
            const result = Game.market.changeOrderPrice(oldOrder.id, oldOrder.price * 0.8);
            switch (result)
            {
                case ERR_NOT_OWNER:
                case ERR_NOT_ENOUGH_RESOURCES:
                case ERR_INVALID_ARGS:
                    console.log("failed to adjust order " + oldOrder.id + " with error " + result);
                    break;
                case OK:
                    Memory.globalAutomationMemory!.handleOldOrders!.push({orderId: oldOrder.id, lastAdjustedTime:Game.time});
                    console.log("dropped price of " + oldOrder.resourceType + " (" + oldOrder.id + ") from " + oldOrder.price + " to " + (oldOrder.price * 0.8));
                    break;
            }
        });

    //if there is a history of old order adjustments, clean out the history of orders that have been closed
    if(Memory.globalAutomationMemory && Memory.globalAutomationMemory.handleOldOrders)
        Memory.globalAutomationMemory.handleOldOrders = Memory.globalAutomationMemory.handleOldOrders.filter(history => _.some(oldOrders, order => order.id === history.orderId));
}
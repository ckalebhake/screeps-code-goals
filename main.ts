import {ErrorMapper} from "utils/ErrorMapper";
import {runBehaviorByName} from "./Behaviors";
import {flagSpecOf} from "./utils/lib.misc";
import {fireTowers} from "./fireTowers";
import {spawnCreeps} from "./spawnCreeps";
import {GoalProviderBasic} from "./GoalProviderBasic";
import {GoalProviderForeignTerritory} from "./GoalProviderForeignTerritory";
import {assignGoals, assignGoalsBeta} from "./assignGoals";
import {autoAdjustSpawnCounts, autoConstruct, autoHarvestFlags, handleOldSellOrders} from "./highLevelAutomation";
import {autoRoomSettings} from "./highLevelAutomation";

//this is the main loop of the AI
//the ErrorMapper is used to print out correct line numbers if something goes wrong

//because the typescript compilation normally mangles these
export const loop = ErrorMapper.wrapLoop(() =>
{
    console.log("=========="); //delineates ticks between each other in the log
    console.log("Screeps-code-goals");

    //confirm presence of required global objects
    if (!Memory.globalSettings)
    {
        Memory.globalSettings = {
            autoConstruct: true,
            autoRoomSettings: true,
            autoHarvestFlags: true,
            autoAdjustSpawnCounts: true,
            handleOldSellOrders: true,

            statisticsTrackingHistoryLength: 2000,
            desiredTerminalEnergyBalance: 30000,
            oldOrderThreshold: 250000,

            defaultLogSettings:
                {
                    availableCreepCount: false,
                    damagedStructureCount: false,
                    debug: false,
                    goalCount: false,
                    goalList: false,
                    goalListVerbose: false,
                    goalUnmetPreferences: false,
                    newAssignments: false,
                    spawning: false,
                    summary: true,
                    taskList: false,
                    market:true,
                    statistics:false,
                },

            moveToReusePathTicks: 5,
        };
    }

    //execute flag orders
    for (const flagName in Game.flags)
    {
        const flag = Game.flags[flagName];

        const spec = flagSpecOf(flag);
        if (spec && spec.order)
            spec.order.run(flag);
    }

    //per-room logic
    for (const roomName in Game.rooms)
    {
        let goals: Goal[] = [];
        const room = Game.rooms[roomName];
        const roomCreeps = room.find(FIND_MY_CREEPS);
        const roomSpawn = room.find(FIND_MY_SPAWNS)[0];

        //confirm room has required memory objects
        if (!room.memory.repairSettings)
        {
            console.log(Game.time + "[" + room.name + "][WARN] no repairSettings specified for room " + room.name + ", defaults provided.");
            room.memory.repairSettings = {maxDefenseHealth: 10000, wallRepairThreshold: 0};
        }

        if (!room.memory.logSettings)
        {
            console.log(Game.time + "[" + room.name + "][WARN] no logSettings specified for room " + room.name + ", defaults provided.");
            room.memory.logSettings = Memory.globalSettings.defaultLogSettings;
        }

        if (!room.memory.spawnSettings)
        {
            console.log(Game.time + "[" + room.name + "][WARN] no spawnSettings specified for room " + room.name + ", defaults provided.");
            room.memory.spawnSettings =
                {
                    targetCreepCounts:
                        {
                            creeps: 7,
                            parts:
                                [
                                    {part: WORK, count: 20},
                                    {part: CARRY, count: 15},
                                ],
                        }
                };
        }

        if (!room.memory.statistics || !room.memory.statistics.averages)
        {
            room.memory.statistics =
                {
                    unspecializedIdleCreepTicks: [],
                    unspecializedCreepTicks: [],
                    upgradesTicks: [],
                    upgradesThisTick: 0,

                    averages:
                        {
                            creeps:0,
                            idle: 0,
                            upgrades:0
                        },

                    roundedAverages:
                        {
                            creeps: 0,
                            idle: 0,
                            upgrades: 0,
                        },
                };
        }

        //if construction is on but the settings are not there, create them
        if (Memory.globalSettings.autoConstruct && !room.memory.constructionSettings)
        {
            console.log("[" + roomName + "] no construction settings found.  defaults provided.");
            room.memory.constructionSettings =
                {
                    forceTheseSettings: false,

                    spawnExtensions: true,
                    roadsBySpawnsAndExtensions: true,
                    roadsByTowers: true,
                    containersBySources: true,
                    containersByExtractors: true,
                    towers: true,
                    extractors: true,
                    storage: true,
                };
        }

        //reset tick upgrade counter
        room.memory.statistics.upgradesThisTick = 0;

        //towers should act first, since they are critical for defense
        fireTowers(room);

        //if any important buildings are damaged, and there are hostile creeps, activate safe mode
        room.find(FIND_MY_STRUCTURES).forEach(structure =>
            {
                const importantTypes = [STRUCTURE_SPAWN, STRUCTURE_STORAGE, STRUCTURE_TOWER, STRUCTURE_LAB];
                if(_.includes(importantTypes, structure.structureType) && room.find(FIND_HOSTILE_CREEPS).length > 0)
                    if(structure.hitsMax && (structure.hits < structure.hitsMax))
                        if (room.controller)
                            room.controller.activateSafeMode();
            });

        //log what is going on in the room
        if (room.memory.logSettings.taskList)
        {
            let taskCounts: any = {idle: 0};
            roomCreeps.forEach(creep =>
            {
                if (creep.memory.goal)
                {
                    if (taskCounts[creep.memory.goal.name])
                        taskCounts[creep.memory.goal.name]++;
                    else
                        taskCounts[creep.memory.goal.name] = 1;
                }
                else
                {
                    taskCounts.idle++;
                }
            });
            console.log(Game.time + " [" + room.name + "] L" + (room.controller || {level: 0}).level + " " + roomCreeps.length + " creeps: " + JSON.stringify(taskCounts));
        }

        // have creeps that are busy keep working on what they were doing,
        // make creeps standing on an exit move off it,
        // and make a list of those that aren't doing anything.
        let availableCreeps: Creep[] = [];
        roomCreeps.forEach(creep =>
        {
            if (creep.memory.goal)
            {
                runBehaviorByName(creep.memory.goal.behaviorName, creep, creep.memory.goal.targetId, creep.memory.goal.value);
                delete creep.memory.idleSince; //creep is doing something, so it is no longer idle
            }
            else
            {
                availableCreeps.push(creep);
            }
        });

        if (room.memory.logSettings.availableCreepCount)
            console.log(Game.time + " [" + roomName + "] " + availableCreeps.length + "/" + roomCreeps.length + " creeps available for new tasks");

        if (availableCreeps.length > 0)
        {
            //gather up the goals
            if (room.controller && room.controller.my)
                goals.push(... GoalProviderBasic.evaluate(room));
            else
                goals.push(...GoalProviderForeignTerritory.evaluate(room));

            room.find(FIND_FLAGS).forEach(flag =>
            {
                const flagSpec = flagSpecOf(flag);
                if (flagSpec && flagSpec.goalProvider)
                    goals.push(...flagSpec.goalProvider.evaluate(flag));
            });

            //log them
            if (room.memory.logSettings.goalCount)
                console.log(Game.time + " [" + roomName + "] " + goals.length + " goals");

            if (room.memory.logSettings.goalList)
                console.log(Game.time + " [" + roomName + "] " + goals.length + " goals:" + JSON.stringify(_.countBy(goals, g => g.name)));

            if (room.memory.logSettings.goalListVerbose)
            {
                const goalStrings: string[] = goals.map(value => value.name + "(" + value.priority + ")");
                console.log("goals:" + JSON.stringify(_.countBy(goalStrings)));
            }

            //assign goals to creeps that aren't busy
            // assignGoals(goals, room, roomName, roomCreeps, availableCreeps);
            assignGoalsBeta(goals, room, roomName, roomCreeps, availableCreeps);
        }
        else if (room.memory.logSettings.goalCount || room.memory.logSettings.goalList || room.memory.logSettings.goalListVerbose)
        {
            console.log(Game.time + " [" + roomName + "] goal counts unavailable because they were not gathered this tick");
        }

        //creeps that are not "specialized" for a specific task.  In other words, these are the general workforce.
        const roomUnspecializedCreeps = roomCreeps.filter(creep => !creep.memory.specialized);

        //gather statistics
        if (room.controller && room.controller.my)
        {
            //use the old average to calculate the sum instead of adding the numbers up every time
            let upgradesSum = room.memory.statistics.averages.upgrades * room.memory.statistics.upgradesTicks.length;
            let unspecializedCreepSum = room.memory.statistics.averages.creeps* room.memory.statistics.unspecializedCreepTicks.length;
            let unspecializedIdleCreepSum = room.memory.statistics.averages.idle * room.memory.statistics.unspecializedIdleCreepTicks.length;

            //update those sums with the new information and update the respective arrays
            upgradesSum +=                            room.memory.statistics.upgradesThisTick;
            room.memory.statistics.upgradesTicks.push(room.memory.statistics.upgradesThisTick);
            unspecializedCreepSum +=                            roomUnspecializedCreeps.length;
            room.memory.statistics.unspecializedCreepTicks.push(roomUnspecializedCreeps.length);
            unspecializedIdleCreepSum +=                            availableCreeps.length;
            room.memory.statistics.unspecializedIdleCreepTicks.push(availableCreeps.length);

            //prune excess statistics entries.  splice() returns the deleted element, so we can perform the deletions and sum updates simultaneously
            if (room.memory.statistics.upgradesTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
                upgradesSum -= room.memory.statistics.upgradesTicks.splice(0, (room.memory.statistics.upgradesTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength))[0];
            if (room.memory.statistics.unspecializedCreepTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
                unspecializedCreepSum -= room.memory.statistics.unspecializedCreepTicks.splice(0, (room.memory.statistics.unspecializedCreepTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength))[0];
            if (room.memory.statistics.unspecializedIdleCreepTicks.length > Memory.globalSettings.statisticsTrackingHistoryLength)
                unspecializedIdleCreepSum -= room.memory.statistics.unspecializedIdleCreepTicks.splice(0, (room.memory.statistics.unspecializedIdleCreepTicks.length - Memory.globalSettings.statisticsTrackingHistoryLength))[0];

            //update statistics
            room.memory.statistics.averages =
                {
                    upgrades: upgradesSum               / room.memory.statistics.upgradesTicks.length,
                    creeps:   unspecializedCreepSum     / room.memory.statistics.unspecializedCreepTicks.length,
                    idle:     unspecializedIdleCreepSum / room.memory.statistics.unspecializedIdleCreepTicks.length,
                };

            room.memory.statistics.roundedAverages =
                {
                    upgrades: Math.round(room.memory.statistics.averages.upgrades),
                    creeps:   Math.round(room.memory.statistics.averages.creeps),
                    idle:     Math.round(room.memory.statistics.averages.idle),
                };
        }
        else
        {
            //room is now unowned.  delete statistics history to save memory.
            delete room.memory.statistics;
        }

        //perform spawning logic after goal assignment so it has data about unmet preferences
        spawnCreeps(roomUnspecializedCreeps, roomSpawn, room);

        //logging
        if (room.memory.logSettings.statistics)
        {
            console.log(Game.time + " [" + room.name + "] rounded averages:" +
                (room.memory.statistics.unspecializedCreepTicks.length < Memory.globalSettings.statisticsTrackingHistoryLength ? "***" : "") +
                JSON.stringify(room.memory.statistics.roundedAverages));
        }

        if (room.memory.logSettings.summary && room.controller && room.controller.my)
        {
            if(room.memory.statistics)
            {
                console.log(Game.time + " [" + room.name + "]" +
                    " L" + (room.controller || {level: 0}).level +
                    " creeps:" + roomCreeps.length + "/" + room.memory.spawnSettings.targetCreepCounts.creeps +
                    ", idle:" + room.find(FIND_MY_CREEPS, {filter: c => !c.memory.goal}).length +
                    ", rounded averages:" + (room.memory.statistics.unspecializedCreepTicks.length < Memory.globalSettings.statisticsTrackingHistoryLength ? "***" : "") +
                    JSON.stringify(room.memory.statistics.roundedAverages) +
                    ((Memory.globalSettings.autoAdjustSpawnCounts) ? ", spawn adjustment: " + room.memory.spawnCountAdjustment + " (next calculation due in " + (Memory.globalSettings.statisticsTrackingHistoryLength - (Game.time - (room.memory.timeOfLastCreepCountAnalysis || 0))) + " ticks)" : ""));
            }
            else
            {
                console.log(Game.time + " [" + roomName + "]: no statistics object!");
            }
        }

        if(room.memory.logSettings.market)
        {
            if(room.memory.sellAtAnyPrice && room.memory.sellAtAnyPrice.length > 0)
                console.log(Game.time + " [" + roomName + "]: selling at any price: " + JSON.stringify(room.memory.sellAtAnyPrice));
        }

        //room high level automation tasks
        if (Memory.globalSettings.autoConstruct && (Game.time % 25 === 0))
            autoConstruct(room);

        if (Memory.globalSettings.autoRoomSettings && (Game.time % 10 === 0))
        {
            autoHarvestFlags(room);
            autoRoomSettings(room);
        }

        if(Memory.globalSettings.autoAdjustSpawnCounts)
        {
            autoAdjustSpawnCounts(room);
        }
    } //end room loop

    //global high level automation tasks
    if(Memory.globalSettings.handleOldSellOrders)
    {
        handleOldSellOrders();
    }

    // Automatically delete memory of missing objects
    for (const name in Memory.creeps)
    {
        if (!(name in Game.creeps))
        {
            delete Memory.creeps[name];
        }
    }

    //flags
    for (const flag in Memory.flags)
        if (!Game.flags[flag])
            delete Memory.flags[flag];


    //room memory cleanup
    for (const roomName in Memory.rooms)
    {
        const roomMemory : RoomMemory = Memory.rooms[roomName];
        if(!roomMemory.lastActiveTick)
          roomMemory.lastActiveTick = Game.time;

        if (Game.rooms[roomName])
        {
          roomMemory.lastActiveTick = Game.time;
        }
        else
        {
          const inactivityCleanupTimeout = 300;
          const cleanupAtTick = roomMemory.lastActiveTick + inactivityCleanupTimeout;
          const timeLeftToCleanup = cleanupAtTick - Game.time;
          console.log(roomName + ": inactive since " + roomMemory.lastActiveTick + " (cleanup in " + timeLeftToCleanup + ")");
          if (timeLeftToCleanup <= 0)
            delete Memory.rooms[roomName];
        }
    }
});

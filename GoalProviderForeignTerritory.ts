import {
    BehaviorAttackController,
    BehaviorAttackCreep,
    BehaviorClaimRoom, BehaviorCleanupDroppedResources,
    BehaviorConstruct,
    BehaviorGoToPosition, BehaviorRepair
} from "./Behaviors";
import {
    PRIORITY_REPAIR_MINOR} from "./goalPriorities";
import {structuresNeedingRepair} from "./utils/lib.room";
import {
    PRIORITY_ATTACK_CONTROLLER,
    PRIORITY_ATTACK_HOSTILE_CIVILIAN,
    PRIORITY_ATTACK_HOSTILE_SOLDIER,
    PRIORITY_ATTACK_HOSTILE_STRUCTURES,
    PRIORITY_ATTACK_HOSTILE_TOWER,
    PRIORITY_CLAIM_ROOM,
    PRIORITY_CLEANUP_RESOURCES,
    PRIORITY_CONSTRUCT,
    PRIORITY_CONSTRUCT_SPAWN,
    PRIORITY_MUSTER, PRIORITY_REPAIR_MODERATE,
    PRIORITY_REPAIR_URGENT, PRIORITY_REPAIR_WALLS
} from "./goalPriorities";

/**
 * Goal provider to be run on rooms that we do not own
 * @type {{}}
 */
export const GoalProviderForeignTerritory : GoalProvider =
{
    evaluate(room: Room) : Goal[]
    {
        const goals : Goal[] = [];

        //either attack the controller or lay claim to it, as appropriate
        if(room.controller)
        {
            if(room.controller.owner)
                goals.push(
                    {
                        name: "attack controller",
                        pos: room.controller.pos,
                        partsRequested: [{part:CLAIM,min:1,preferred:3}],
                        priority: PRIORITY_ATTACK_CONTROLLER,
                        behaviorName: BehaviorAttackController.name,
                        maximumCreepCount:1, //because there is a cooldown between attacks on the controller
                    }
                );
            else
                goals.push(
                    {
                        name: "claim room",
                        pos: room.controller.pos,
                        partsRequested: [{part:CLAIM}],
                        priority: PRIORITY_CLAIM_ROOM,
                        behaviorName: BehaviorClaimRoom.name,
                        maximumCreepCount:1
                    }
                );
        }

        //if there are hostile creeps in the room, attack them
        const hostileCreeps = room.find(FIND_HOSTILE_CREEPS);
        if(hostileCreeps && hostileCreeps.length > 0)
        {
            hostileCreeps.forEach(hostileCreep =>
            {
                goals.push(
                    {
                        name: "attack creep",
                        pos: hostileCreep.pos,
                        targetId: hostileCreep.id,
                        partsRequested: [{part:ATTACK}],
                        priority: hostileCreep.body.some(part => part.type === ATTACK || part.type === CLAIM) ? PRIORITY_ATTACK_HOSTILE_SOLDIER : PRIORITY_ATTACK_HOSTILE_CIVILIAN,
                        behaviorName: BehaviorAttackCreep.name,
                    }
                );
            })
        }

        //if there are hostile structures in the room, attack them
        const hostileStructures = room.find(FIND_HOSTILE_STRUCTURES);
        if(hostileStructures && hostileStructures.length > 0)
        {
            hostileStructures.forEach(hostileStructure =>
            {
                //skip the controller since we already have another goal for that
                if(hostileStructure.structureType === STRUCTURE_CONTROLLER)
                    return;

                goals.push(
                    {
                        name: "attack structure",
                        pos: hostileStructure.pos,
                        targetId: hostileStructure.id,
                        partsRequested: [{part:ATTACK}],
                        priority: hostileStructure.structureType === STRUCTURE_TOWER ? PRIORITY_ATTACK_HOSTILE_TOWER : PRIORITY_ATTACK_HOSTILE_STRUCTURES,
                        behaviorName: BehaviorAttackCreep.name,
                    }
                );
            })
        }

        //if there are hostile construction sites here, step on the m
        const enemyConstructionSites = room.find(FIND_HOSTILE_CONSTRUCTION_SITES);
        if (enemyConstructionSites && enemyConstructionSites.length > 0)
        {
            enemyConstructionSites.forEach(enemyConstructionSite =>
            {
                goals.push(
                    {
                        name: "stomp enemy construction site",
                        pos: enemyConstructionSite.pos,
                        targetId: enemyConstructionSite.id,
                        partsRequested: [],
                        priority: PRIORITY_MUSTER,
                        behaviorName: BehaviorGoToPosition.name,
                        maximumCreepCount: 1,
                    });
            });
        }

        //complete any construction sites in the room
        const constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES);
        constructionSites.forEach(site =>
        {
            //bump up priority for some types of structures
            let priority: number = PRIORITY_CONSTRUCT;
            switch (site.structureType)
            {
                case STRUCTURE_SPAWN:
                    priority = PRIORITY_CONSTRUCT_SPAWN;
                    break;
                case STRUCTURE_TOWER:
                    priority += 0.8;
                    break;
                case STRUCTURE_WALL:
                    priority += 0.7;
                    break;
                case STRUCTURE_CONTAINER:
                    priority += 0.6;
                    break;
                case STRUCTURE_EXTENSION:
                    priority += 0.5;
                    break;

                default:
                    break;
            }

            //also nudge priority based on construction progress, but not enough to outweigh the above rankings
            priority += (site.progress / site.progressTotal) / 10;

            goals.push(
                {
                    name: "Construct " + site.structureType.toString(),
                    pos: site.pos,
                    minimumEnergy: 10,
                    prefersEnergy: site.progressTotal - site.progress,
                    partsRequested: [{part: WORK}, {part: CARRY}],
                    priority: priority,
                    behaviorName: BehaviorConstruct.name,
                    targetId: site.id,
                    maximumCreepCount: (site.structureType === STRUCTURE_WALL) ? 1 : undefined, //walls are cheap and should only be built by one creep
                }
            );
        });

        //repair any damaged structures
        structuresNeedingRepair(room).forEach(structure =>
        {
            //determine priority
            const fractionHealth = (structure.hits / structure.hitsMax);
            const percentHealth = fractionHealth * 100;
            let priority: number;
            if (structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART)
            {
                priority = PRIORITY_REPAIR_WALLS; //walls get their own priority because of their massive health pool
            }
            else
            {
                //for everything else, base it on how damaged they are
                if (percentHealth < 30)
                    priority = PRIORITY_REPAIR_URGENT;
                else if (percentHealth < 70)
                    priority = PRIORITY_REPAIR_MODERATE;
                else
                    priority = PRIORITY_REPAIR_MINOR;
            }

            //create the goal
            goals.push(
                {
                    name: "Repair " + structure.structureType.toString(),
                    pos: structure.pos,
                    minimumEnergy: 10,
                    prefersEnergy: (structure.hitsMax - structure.hits) / 7.5,
                    partsRequested: [{part: WORK}, {part: CARRY}],
                    priority: priority + (1 - fractionHealth), //the (1 - fractionHealth) part weights repairs of the smae priority tier so most damaged structures go first
                    behaviorName: BehaviorRepair.name,
                    targetId: structure.id,
                    maximumCreepCount: 1,
                }
            );
        });

        //if there is dropped resources on the ground, pick them up
        const droppedResources = room.find(FIND_DROPPED_RESOURCES);
        if (droppedResources && droppedResources.length > 0)
        {
            droppedResources.forEach(droppedResource =>
            {
                goals.push(
                    {
                        name: "cleanup dropped resources",
                        pos: droppedResource.pos,
                        targetId: droppedResource.id,
                        partsRequested: [{part: CARRY}],
                        priority: PRIORITY_CLEANUP_RESOURCES,
                        behaviorName: BehaviorCleanupDroppedResources.name,
                        minimumOpenCapacity: 10,
                        prefersOpenCapacity: droppedResource.amount,
                    }
                )
            })
        }

        return goals;
    }
};
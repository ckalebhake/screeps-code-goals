import {BehaviorIdle, runBehaviorByName} from "./Behaviors";
import {countCreepsWithGoal, isCreepPreferred, isCreepQualified} from "./utils/lib.creep";


/**
 * assigns the given creep to the given goal
 * @param {Creep} creep
 * @param {Goal} goal
 */
function assignGoal(creep: Creep, goal: Goal)
{
    creep.memory.goal = goal;
    runBehaviorByName(goal.behaviorName, creep, goal.targetId);

    if (creep.room.memory.logSettings.newAssignments)
        console.log(Game.time + " [" + creep.room.name + "] " + "new assignment: " + creep.name + "=>" + goal.name);
}

/**
 * assigns goals to creeps
 * @param {Goal[]} goals to assign
 * @param {Room} room room the goals are in
 * @param {string} roomName the name of the room
 * @param {Creep[]} roomCreeps creeps in the room
 * @param {Creep[]} availableCreeps creeps in the room that aren't busy
 */
export function assignGoals(goals: Goal[], room: Room, roomName: string, roomCreeps: Creep[], availableCreeps: Creep[])
{
    //sort the goals
    goals = _.sortBy(goals, value => value.priority).reverse();

    //for each goal...
    for (let i = 0; i < goals.length; i++)
    {
        const goal = goals[i];

        //bail early if no creeps available
        if (availableCreeps.length === 0)
            return;

        const goalCreepCounts = countCreepsWithGoal(room, goal.name, goal.targetId);

        //skip goals that have already hit the maximum number of units
        if (goal.maximumCreepCount && goal.maximumCreepCount <= goalCreepCounts.creeps)
            continue;

        //skip goals that have already hit the maximum number of parts
        if (goal.partsRequested.some(request =>
            {
                //we cant be at the max if there is no max
                if (!request.max)
                    return false;

                //retrieve the part count for this specific kind of part
                const partCount = _.find(goalCreepCounts.parts, creepCount => creepCount.part === request.part);

                //we cant be at the max if we have none of this part
                if (!partCount)
                    return false;

                return partCount.count >= request.max;
            }))
        {
            continue;
        }

        //apply goal-specific filtering to rule out creeps who don't qualify for the task at hand.
        let eligibleCreeps: Creep[] = availableCreeps.filter(creep => isCreepQualified(creep, goal));

        //if there are any creeps that match the goal's preferences, then choose only from those
        const preferredCreeps: Creep[] = eligibleCreeps.filter(creep => isCreepPreferred(creep, goal));
        if (preferredCreeps.length > 0)
            eligibleCreeps = preferredCreeps;


        //sort the creeps by distance to the goal
        eligibleCreeps = _.sortBy(eligibleCreeps, creep => creep.pos.getRangeTo(goal.pos));

        //take the closest creep in the list and assign it to the goal
        const selectedCreep = goal.pos.findClosestByPath(eligibleCreeps);

        //no creeps are available to perform the goal
        if (!selectedCreep)
        {
            //some goals request specific kinds of units if their requirements cant be met
            if (goal.ifRequirementsUnmetSpawn)
            {
                const spawnToRequestFrom = room.find(FIND_MY_SPAWNS)[0];
                if (spawnToRequestFrom)
                    if (!_.contains(spawnToRequestFrom.memory.unmetPreferences, goal.ifRequirementsUnmetSpawn))
                        spawnToRequestFrom.memory.unmetPreferences.push(goal.ifRequirementsUnmetSpawn);
            }

            continue;
        }

        //make the assignment
        //i--; //dirty hacky behavior: bring the loop back an iteration so this goal gets checked again.  this fixes a bug where only one creep could be assigned to a given goal per tick
        assignGoal(selectedCreep, goal);
        availableCreeps.splice(availableCreeps.indexOf(selectedCreep), 1);
    }

    //goal assignment complete.  if any are still idle, have them run the idle behavior
    //this behavior is special: it is run only when there is no goal.
    availableCreeps.forEach(creep => BehaviorIdle.run(creep));
}

/**
 * assigns goals to creeps
 * @param {Goal[]} goals to assign
 * @param {Room} room room the goals are in
 * @param {string} roomName the name of the room
 * @param {Creep[]} roomCreeps creeps in the room
 * @param {Creep[]} availableCreeps creeps in the room that aren't busy
 */
export function assignGoalsBeta(goals: Goal[], room: Room, roomName: string, roomCreeps: Creep[], availableCreeps: Creep[])
{
    //skip goals that have already hit their maximums
    _.remove(goals, goal =>
    {
        const goalCreepCounts = countCreepsWithGoal(room, goal.name, goal.targetId);

        //skip goals that are already at their maximum creep counts
        if (goal.maximumCreepCount && goal.maximumCreepCount <= goalCreepCounts.creeps)
            return true;

        //skip goals that have already hit the maximum number of parts
        return goal.partsRequested.some(request =>
        {
            //careful here: return statements below are for the .some() lambda, and not the .remove() lmabda!

            //we cant be at the max if there is no max
            if (!request.max)
                return false;

            //retrieve the part count for this specific kind of part
            const partCount = _.find(goalCreepCounts.parts, creepCount => creepCount.part === request.part);

            //we cant be at the max if we have none of this part
            if (!partCount)
                return false;

            return partCount.count >= request.max;
        });
    });

    //sort the goals
    goals = _.sortBy(goals, value => value.priority).reverse();

    while (goals.length > 0)
    {
        // console.log(Game.time + " [" + roomName + "] " + JSON.stringify(goals)); //DEBUG

        //split out all of the highest priority goals with the same name
        const priorityGoals = _.remove(goals, g => g.priority === goals[0].priority && g.name === goals[0].name);
        const qualifiedCreeps = availableCreeps.filter(creep => isCreepQualified(creep, priorityGoals[0]));

        //compile a list of all possible assignments between these creeps and goals
        let possibleAssignments : {creep: Creep, goal: Goal, distance: number, preferred: boolean}[] = [];
        qualifiedCreeps.forEach(creep =>
        {
            priorityGoals.forEach(goal =>
            {
                possibleAssignments.push({creep: creep, goal: goal, distance: creep.pos.getRangeTo(goal.pos), preferred: isCreepPreferred(creep, goal)});
            });
        });

        //sort the list with preferred pairings first, and then by lowest distance
        possibleAssignments = _.sortBy(possibleAssignments, pairing =>
            {
                let sortValue = pairing.distance;
                if (pairing.preferred)
                    sortValue -= 9999;
                return sortValue;
            });

        //make as many assignments as possible from that list
        while (possibleAssignments.length > 0)
        {
            const assignment = possibleAssignments[0];     //fetch the assignment
            assignGoal(assignment.creep, assignment.goal); //perform the assigment

            //prevent this creep and goal from being used in future assignments this tick
            _.remove(availableCreeps, assignment.creep);
            _.remove(possibleAssignments, pa => pa.creep === assignment.creep || pa.goal === assignment.goal);
        }
    }

    //goal assignment complete.  if any are still idle, have them run the idle behavior
    //this behavior is special: it is run only when there is no goal.
    availableCreeps.forEach(creep => BehaviorIdle.run(creep));
}


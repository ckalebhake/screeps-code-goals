import {structuresNeedingRepair} from "./utils/lib.room";

export function fireTowers(room: any)
{
    //find all towers in the room
    const towers: StructureTower[] = <StructureTower[]>room.find(FIND_MY_STRUCTURES,
        {
            filter: function (structure: Structure)
            {
                return structure.structureType === STRUCTURE_TOWER;
            }
        });

    //for each tower
    for (let i in towers)
    {
        const tower: StructureTower = towers[i];

        //if there are enemies in the room, shoot them
        const enemies: Creep[] = room.find(FIND_HOSTILE_CREEPS);
        if (enemies.length > 0)
        {
            //sort the list, prioritizing first by number of heal parts and then by with lowest health
            enemies.sort(function (enemyA: Creep, enemyB: Creep)
            {
                const enemyAHealParts = enemyA.body.reduce(function (count, part)
                {
                    if (part.type === HEAL) return count + 1;
                    else return count;
                }, 0);
                const enemyBHealParts = enemyB.body.reduce(function (count, part)
                {
                    if (part.type === HEAL) return count + 1;
                    else return count;
                }, 0);

                const healCompare = enemyBHealParts - enemyAHealParts;
                if (healCompare !== 0)
                    return healCompare;

                return enemyA.hits - enemyB.hits;
            });

            //attack the highest priority enemy
            tower.attack(enemies[0]);
        }
        else
        {
            //repair structures
            if (tower.energy > 800) //only if well-supplied, so that we definitely have power for defense if we need it
            {
                //finds all structures that should be repaired
                //note that this is more complicated than just structures that are damaged
                //most notably, targets may be filtered out from this list based on the room's repairSettings
                const damagedStructures = structuresNeedingRepair(room);

                //filter the list even further, to only target structures in urgent need
                //this is because creeps are generally more efficient repairmen
                const targets: Structure[] = damagedStructures.filter((damagedStructure: Structure) =>
                {
                    //towers should not repair walls as it causes economy problems
                    if (damagedStructure.structureType === STRUCTURE_WALL)
                        return false;

                    //for ramparts, consider the max health to be the value in repairSettings.  For others, consider it their max health
                    let maxHealth = 0;
                    if (damagedStructure.structureType === STRUCTURE_RAMPART)
                        maxHealth = room.memory.repairSettings.maxDefenseHealth;
                    else
                        maxHealth = damagedStructure.hitsMax;

                    //only repair things below 30% health
                    return ((damagedStructure.hits / maxHealth) * 100) < 30;
                });

                if (targets.length)
                {
                    //find the most urgent target to repair
                    let mostUrgent = null;
                    let lowestHits = 999999999999;
                    for (const target in targets)
                    {
                        if (targets[target].hits < lowestHits)
                        {
                            lowestHits = targets[target].hits;
                            mostUrgent = targets[target];
                        }
                    }

                    //repair it
                    if (mostUrgent)
                    {
                        tower.repair(mostUrgent);
                    }
                }
            }
        }
    }
}
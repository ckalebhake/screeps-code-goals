import {
  BehaviorAttackCreep,
  BehaviorCleanupDroppedResources, BehaviorCollectEnergyFromStorage, BehaviorCollectEnergyFromTerminal,
  BehaviorCollectFromContainer,
  BehaviorConstruct,
  BehaviorGoToPosition,
  BehaviorRepair, BehaviorSellResourceAtAnyPrice, BehaviorStoreResource,
  BehaviorSupplySpawnOrExtension, BehaviorSupplyTerminal, BehaviorTransferResourceToRoom,
  BehaviorUpgrade
} from "./Behaviors";
import {roomContainers, roomSpawnsAndExtensions, structuresNeedingRepair} from "./utils/lib.room";
import {
    PRIORITY_COLLECT_STORAGE} from "./goalPriorities";
import {
    PRIORITY_ATTACK_HOSTILE_CIVILIAN,
    PRIORITY_ATTACK_HOSTILE_SOLDIER,
    PRIORITY_CLEANUP_RESOURCES, PRIORITY_COLLECT,
    PRIORITY_COLLECT_URGENT,
    PRIORITY_CONSTRUCT,
    PRIORITY_CONSTRUCT_SPAWN,
    PRIORITY_MUSTER, PRIORITY_REPAIR_MINOR, PRIORITY_REPAIR_MODERATE,
    PRIORITY_REPAIR_URGENT, PRIORITY_REPAIR_WALLS,
    PRIORITY_STORE_RESOURCE,
    PRIORITY_SUPPLY,
    PRIORITY_SUPPLY_TERMINAL,
    PRIORITY_TRADE, PRIORITY_UPGRADE_EXTRA,
    PRIORITY_UPGRADE_MAINTAIN,
    PRIORITY_UPGRADE_URGENT
} from "./goalPriorities";

//this identifies basic goals present in every room and evaluates which should be performed

/**
 * provides goals for basic tasks that must be performed in every room
 * @type {{evaluate(room: Room): Goal[]}}
 */
export const GoalProviderBasic: GoalProvider =
    {
        evaluate(room: Room): Goal[]
        {
            //init
            let goals: Goal[] = [];

            //if the room has a controller, it should be upgraded
            //this task is more urgent if the room is about to downgrade
            //we create a goal for one creep at either the priority of UPGRADE_URGENT or UPGRADE_MAINTAIN, based on if the room is about to downgrade
            //additional creeps can perform upgrading, but this is less important and so uses priority UPGRADE_EXTRA.
            if (room.controller)
            {
                //primary upgrade goal
                goals.push(
                    {
                        name: "Upgrade controller",
                        pos: room.controller.pos,
                        minimumEnergy: 10,
                        partsRequested: [{part: WORK}, {part: CARRY}],
                        priority: (room.controller.ticksToDowngrade < 4000) ? PRIORITY_UPGRADE_URGENT : PRIORITY_UPGRADE_MAINTAIN,
                        behaviorName: BehaviorUpgrade.name,
                        maximumCreepCount: 1,
                    });

                //lower priority goal to catch creeps with nothing better to do
                goals.push(
                    {
                        name: "Upgrade controller",
                        pos: room.controller.pos,
                        minimumEnergy: 10,
                        partsRequested: [{part: WORK}, {part: CARRY}],
                        priority: PRIORITY_UPGRADE_EXTRA,
                        behaviorName: BehaviorUpgrade.name,
                    });
            }

            //if the spawn and its extensions are not full, create a goal to supply each such structure
            roomSpawnsAndExtensions(room).forEach((s: StructureSpawn | StructureExtension) =>
            {
                if (s.energy < s.energyCapacity)
                    goals.push(
                        {
                            name: "Supply spawn",
                            pos: s.pos,
                            minimumEnergy: 25,
                            partsRequested: [{part: CARRY}],
                            priority: PRIORITY_SUPPLY,
                            behaviorName: BehaviorSupplySpawnOrExtension.name,
                            targetId: s.id,
                            maximumCreepCount: 1,
                        });
            });


            //complete any construction sites in the room
            const constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES);
            constructionSites.forEach(site =>
            {
                //bump up priority for some types of structures
                let priority: number = PRIORITY_CONSTRUCT;
                switch (site.structureType)
                {
                    case STRUCTURE_SPAWN:
                        priority = PRIORITY_CONSTRUCT_SPAWN;
                        break;
                    case STRUCTURE_TOWER:
                        priority += 0.8;
                        break;
                    case STRUCTURE_WALL:
                        priority += 0.7;
                        break;
                    case STRUCTURE_CONTAINER:
                    case STRUCTURE_STORAGE:
                        priority += 0.6;
                        break;
                    case STRUCTURE_EXTENSION:
                        priority += 0.5;
                        break;

                    default:
                        break;
                }

                //also nudge priority based on construction progress, but not enough to outweigh the above rankings
                priority += (site.progress / site.progressTotal) / 10;

                goals.push(
                    {
                        name: "Construct " + site.structureType.toString(),
                        pos: site.pos,
                        minimumEnergy: 10,
                        prefersEnergy: site.progressTotal - site.progress,
                        partsRequested: [{part: WORK}, {part: CARRY}],
                        priority: priority,
                        behaviorName: BehaviorConstruct.name,
                        targetId: site.id,
                        maximumCreepCount: (site.structureType === STRUCTURE_WALL) ? 1 : undefined, //walls are cheap and should only be built by one creep
                    }
                );
            });

            //repair any damaged structures
            structuresNeedingRepair(room).forEach(structure =>
            {
                //determine priority
                const fractionHealth = (structure.hits / structure.hitsMax);
                const percentHealth = fractionHealth * 100;
                let priority: number;
                if (structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART)
                {
                    priority = PRIORITY_REPAIR_WALLS; //walls get their own priority because of their massive health pool
                }
                else
                {
                    //for everything else, base it on how damaged they are
                    if (percentHealth < 30)
                        priority = PRIORITY_REPAIR_URGENT;
                    else if (percentHealth < 70)
                        priority = PRIORITY_REPAIR_MODERATE;
                    else
                        priority = PRIORITY_REPAIR_MINOR;
                }

                //create the goal
                goals.push(
                    {
                        name: "Repair " + structure.structureType.toString(),
                        pos: structure.pos,
                        minimumEnergy: 10,
                        prefersEnergy: (structure.hitsMax - structure.hits) / 7.5,
                        partsRequested: [{part: WORK}, {part: CARRY}],
                        priority: priority + (1 - fractionHealth), //the (1 - fractionHealth) part weights repairs of the smae priority tier so most damaged structures go first
                        behaviorName: BehaviorRepair.name,
                        targetId: structure.id,
                        maximumCreepCount: 1,
                    }
                );
            });

            //collect from containers
            roomContainers(room).forEach(container =>
            {
                const containerPercent = (_.sum(container.store) / container.storeCapacity) * 100;
                if (containerPercent > 10)
                {
                    const targetResource: ResourceConstant = <ResourceConstant>_.max(_.keys(container.store), resource =>
                    {
                        //if the room has no storage, don't consider resources other than energy
                        if(resource === RESOURCE_ENERGY || room.storage)
                            return (container.store as any)[resource];
                        else
                            return -1;
                    });

                    //only push the goal if there is still something to collect after the above filtering
                    if (container.store[targetResource] && container.store[targetResource]! > 0)
                    {
                        goals.push(
                            {
                                name: "Collect from container",
                                pos: container.pos,
                                minimumOpenCapacity: 25,
                                prefersOpenCapacity: container.store[targetResource],
                                partsRequested: [{part: CARRY, preferred: Math.ceil( (container.store[targetResource]!) / 100)}],
                                priority: ((container.store[targetResource]!) > 1400) ? PRIORITY_COLLECT_URGENT : PRIORITY_COLLECT,
                                behaviorName: BehaviorCollectFromContainer.name,
                                targetId: container.id,
                            }
                        )
                    }
                }
            });

            //collect energy from storage
            if(room.storage && room.storage.store.energy && room.storage.store.energy >= 1)
            {
                goals.push(
                    {
                        name: "Collect energy from storage",
                        pos: room.storage.pos,
                        minimumOpenCapacity: 25,
                        prefersOpenCapacity: room.storage.store[RESOURCE_ENERGY],
                        partsRequested: [{part: CARRY, preferred: Math.ceil(room.storage.store.energy / 100)}],
                        priority: PRIORITY_COLLECT_STORAGE,
                        behaviorName: BehaviorCollectEnergyFromStorage.name,
                        targetId: room.storage.id,
                    }
                )
            }

            //if there are hostile creeps in the room, attack them
            const hostileCreeps = room.find(FIND_HOSTILE_CREEPS);
            if (hostileCreeps && hostileCreeps.length > 0)
            {
                hostileCreeps.forEach(hostileCreep =>
                {
                    goals.push(
                        {
                            name: "attack creep",
                            pos: hostileCreep.pos,
                            targetId: hostileCreep.id,
                            partsRequested: [{part: ATTACK}],
                            priority: hostileCreep.body.some(part => part.type === ATTACK || part.type === CLAIM) ? PRIORITY_ATTACK_HOSTILE_SOLDIER : PRIORITY_ATTACK_HOSTILE_CIVILIAN,
                            behaviorName: BehaviorAttackCreep.name,
                            //if this is our room and there are no creeps to defend, spawn one.  base the body on the level of the room.
                            ifRequirementsUnmetSpawn: "Soldier",
                        }
                    );
                })
            }

            //if there are dropped resources on the ground, pick them up
            const droppedResources = room.find(FIND_DROPPED_RESOURCES);
            if (droppedResources && droppedResources.length > 0)
            {
                droppedResources.forEach(droppedResource =>
                {
                    if (droppedResource.resourceType === RESOURCE_ENERGY || room.storage) //only cleanup minerals if we have somewhere to put them
                    {
                        goals.push(
                            {
                                name: "cleanup dropped resources",
                                pos: droppedResource.pos,
                                targetId: droppedResource.id,
                                partsRequested: [{part: CARRY}],
                                priority: PRIORITY_CLEANUP_RESOURCES,
                                behaviorName: BehaviorCleanupDroppedResources.name,
                                minimumOpenCapacity: 10,
                                prefersOpenCapacity: droppedResource.amount,
                            }
                        )
                    }
                })
            }

            //if there are hostile construction sites here, step on the m
            const enemyConstructionSites = room.find(FIND_HOSTILE_CONSTRUCTION_SITES);
            if (enemyConstructionSites && enemyConstructionSites.length > 0)
            {
                enemyConstructionSites.forEach(enemyConstructionSite =>
                {
                    goals.push(
                        {
                            name: "stomp enemy construction site",
                            pos: enemyConstructionSite.pos,
                            targetId: enemyConstructionSite.id,
                            partsRequested: [],
                            priority: PRIORITY_MUSTER,
                            behaviorName: BehaviorGoToPosition.name,
                            maximumCreepCount: 1,
                        });
                });
            }

            //deposit held resources in storage, if you have nothing better to do with them
            if(room.storage)
            {
                goals.push({
                    name: "store held resource",
                    pos: room.storage.pos,
                    targetId: room.storage.id,
                    partsRequested: [],
                    priority: PRIORITY_STORE_RESOURCE,
                    behaviorName: BehaviorStoreResource.name,
                    maximumCreepCount: 1,
                    minimumMinerals: 1,
                })
            }

            //empty out any tombstones in the room
            //@ts-ignore (the typescript starter doesnt have FIND_TOMBSTONES, which is a valid parameter.  that constant's value is 118)
            const roomTombstones : Tombstone[] = <Tombstone[]>room.find(118);
            roomTombstones.forEach(tombstone =>
            {
                if(_.sum(tombstone.store) > 0)
                {
                    goals.push({
                        name: "cleanup tombstone",
                        pos: tombstone.pos,
                        targetId: tombstone.id,
                        partsRequested: [{part: CARRY}],
                        priority: PRIORITY_CLEANUP_RESOURCES,
                        behaviorName: BehaviorCollectFromContainer.name,
                        minimumOpenCapacity: 10,
                        prefersOpenCapacity: _.sum(tombstone.store),
                        maximumCreepCount: 1,
                    })
                }
            });

            //maintain a balance of exactly Memory.globalSettings.desiredTerminalEnergyBalance energy in terminals
            if(room.terminal && room.terminal.store.energy < Memory.globalSettings.desiredTerminalEnergyBalance)
                goals.push({
                    name: "supply terminal",
                    pos: room.terminal.pos,
                    partsRequested: [{part: CARRY}],
                    priority: PRIORITY_SUPPLY_TERMINAL,
                    behaviorName: BehaviorSupplyTerminal.name,
                    minimumEnergy: 25,
                    prefersEnergy: (Memory.globalSettings.desiredTerminalEnergyBalance - room.terminal.store.energy),
                    maximumCreepCount: 1,
                });
            if (room.terminal && room.terminal.store.energy > Memory.globalSettings.desiredTerminalEnergyBalance)
                goals.push({
                    name: "collect from terminal",
                    pos: room.terminal.pos,
                    partsRequested: [{part: CARRY}],
                    priority: PRIORITY_COLLECT,
                    behaviorName: BehaviorCollectEnergyFromTerminal.name,
                    minimumOpenCapacity: 25,
                    prefersOpenCapacity: (room.terminal.store.energy - Memory.globalSettings.desiredTerminalEnergyBalance),
                    maximumCreepCount: 1,
                });

            //execute queued sellAtAnyPrice orders
          if(room.memory.sellAtAnyPrice && room.memory.sellAtAnyPrice.length > 0)
            {
                const sellRequest = room.memory.sellAtAnyPrice[0];
                //there is a terminal with the proper balance
                if (room.terminal && room.terminal.store.energy >= Memory.globalSettings.desiredTerminalEnergyBalance &&
                    //and a storage structure containing resource to trade
                    room.storage && room.storage.store[sellRequest.resource] &&
                    //and we have enough of it
                    ((room.terminal.store[sellRequest.resource] || 0) + (room.storage.store[sellRequest.resource] || 0)) >= sellRequest.amount)
                {
                    goals.push({
                        name: "sell at any price",
                        pos: room.terminal.pos,
                        partsRequested: [{part: CARRY}],
                        priority: PRIORITY_TRADE,
                        behaviorName: BehaviorSellResourceAtAnyPrice.name,
                        minimumOpenCapacity: 100,
                        prefersOpenCapacity: sellRequest.amount,
                        maximumCreepCount: 1,
                        targetId: sellRequest.resource,
                        value: sellRequest.amount,
                    });
                }
            }

          //execute queued transferResources orders
          if(room.memory.transferResources && room.memory.transferResources.length > 0)
          {
            const transferRequest = room.memory.transferResources[0];
            //there is a terminal with the proper balance
            if (room.terminal && room.terminal.store.energy >= Memory.globalSettings.desiredTerminalEnergyBalance &&
              //and a storage structure containing resource to transfer
              room.storage && room.storage.store[transferRequest.resource] &&
              //and we have enough of it
              ((room.terminal.store[transferRequest.resource] || 0) + (room.storage.store[transferRequest.resource] || 0)) >= transferRequest.amount)
            {
              goals.push({
                name: "transfer resource to room",
                pos: room.terminal.pos,
                partsRequested: [{part: CARRY}],
                priority: PRIORITY_TRADE,
                behaviorName: BehaviorTransferResourceToRoom.name,
                minimumOpenCapacity: 100,
                prefersOpenCapacity: transferRequest.amount,
                maximumCreepCount: 1,
                targetId: transferRequest.resource + "::" + transferRequest.destination,
                value: transferRequest.amount,
              });
            }
          }

            //supply inactive sell orders
            const inactiveSellOrdersInRoom = _.filter(Game.market.orders, order => (!order.active) && (order.roomName === room.name));
            if(inactiveSellOrdersInRoom.length > 0)
            {
                console.log(Game.time + " [" + room.name + "] " + inactiveSellOrdersInRoom.length + " inactive sell orders");
                //TODO: supply these inactive orders
            }

            //TODO: add goals to perform the following kinds of trades: buy at any price, sell at specific price, buy at specific price

            return goals;
        }
    };

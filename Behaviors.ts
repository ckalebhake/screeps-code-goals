//Behaviors are pieces of code attached to goals.
//they form the discrete tasks that a creep can perform.
import {idlePos, roomContainers, roomContainersAndStorage} from "./utils/lib.room";
import {moveToOptions} from "./utils/lib.creep";
import {PRIORITY_STORE_RESOURCE} from "./goalPriorities";
import {flagSpecOf} from "./utils/lib.misc";
//TODO: cleanup reuse
/**
 * runs the behavior with the given name
 * @param {string} name name of the behavior
 * @param {Creep} creep creep to act
 * @param {string} targetId ID of the target object.  the meaning of this varies by behavior.
 * @param value
 */
export function runBehaviorByName(name: string, creep: Creep, targetId?: string, value?: number): void
{
    const nameMap: any =
        {
            "BehaviorAttackController"          : BehaviorAttackController,
            "BehaviorAttackCreep"               : BehaviorAttackCreep,
            "BehaviorClaimRoom"                 : BehaviorClaimRoom,
            "BehaviorCleanupDroppedResources"   : BehaviorCleanupDroppedResources,
            "BehaviorCollectEnergyFromStorage"  : BehaviorCollectEnergyFromStorage,
            "BehaviorCollectEnergyFromTerminal" : BehaviorCollectEnergyFromTerminal,
            "BehaviorCollectFromContainer"      : BehaviorCollectFromContainer,
            "BehaviorCollectFromLab"            : BehaviorCollectFromLab,
            "BehaviorConstruct"                 : BehaviorConstruct,
            "BehaviorGoToPosition"              : BehaviorGoToPosition,
            "BehaviorGoToRoom"                  : BehaviorGoToRoom,
            "BehaviorHarvestEnergy"             : BehaviorHarvestEnergy,
            "BehaviorHarvestToContainer"        : BehaviorHarvestToContainer,
            "BehaviorMuster"                    : BehaviorMuster,
            "BehaviorRepair"                    : BehaviorRepair,
            "BehaviorSellResourceAtAnyPrice"    : BehaviorSellResourceAtAnyPrice,
            "BehaviorStoreResource"             : BehaviorStoreResource,
            "BehaviorSupplyLabWithMineral"      : BehaviorSupplyLabWithMineral,
            "BehaviorSupplySpawnOrExtension"    : BehaviorSupplySpawnOrExtension,
            "BehaviorSupplyTerminal"            : BehaviorSupplyTerminal,
            "BehaviorTransferResourceToRoom"    : BehaviorTransferResourceToRoom,
            "BehaviorTransportMinerals"         : BehaviorTransportMinerals,
            "BehaviorTransportEnergy"           : BehaviorTransportEnergy,
            "BehaviorUpgrade"                   : BehaviorUpgrade,
        };

    const behavior: Behavior = <Behavior>nameMap[name];
    if (behavior)
        behavior.run(creep, targetId, value);
    else
    {
        console.log(Game.time + "[" + creep.room.name + "][ERROR]: could not find behavior " + name + ".  Removing malformed goal on creep " + creep.name);
        delete creep.memory.goal;
    }
}

/**
 * upgrades the controller
 * @type {{name: string; run(creep: Creep): void}}
 */
export const BehaviorUpgrade: Behavior =
    {
        name: "BehaviorUpgrade",
        run(creep: Creep)
        {
            if (creep.room.controller)
            {
                const upgradeResult = creep.upgradeController(creep.room.controller);
                if (upgradeResult === ERR_NOT_IN_RANGE)
                    creep.moveTo(creep.room.controller, moveToOptions(this));
                else if(upgradeResult === OK)
                    creep.room.memory.statistics.upgradesThisTick++;
            }

            if (creep.carry.energy === 0)
                delete creep.memory.goal;
        }
    };

/**
 * harvests energy directly from a source and does nothing special with it
 * @type {{name: string; run(creep: Creep, targetSourceId?: string): undefined}}
 */
export const BehaviorHarvestEnergy: Behavior =
    {
        name: "BehaviorHarvestEnergy",
        run(creep: Creep, targetSourceId?: string)
        {
            let targetSource = null;
            if (targetSourceId)
                targetSource = <Source>Game.getObjectById(targetSourceId);
            else
                targetSource = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);

            if (!targetSource)
            {
                console.log("[ERROR]: BehaviorHarvestEnergy can't find an active source");
                return;
            }

            if (creep.harvest(targetSource) === ERR_NOT_IN_RANGE)
                creep.moveTo(targetSource, moveToOptions(this));

            if (_.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * harvests energy directly from a source and stores it in a container
 * @type {{name: string; run(creep: Creep, targetSourceId?: string): undefined}}
 */
export const BehaviorHarvestToContainer: Behavior =
    {
        name: "BehaviorHarvestToContainer",
        run(creep: Creep, targetSourceId?: string)
        {
            if (!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {state: "HARVEST"};

            let targetSourceOrMineral = null;
            if (targetSourceId)
                targetSourceOrMineral = <Source | Mineral>Game.getObjectById(targetSourceId);
            else
                targetSourceOrMineral = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);

            if (!targetSourceOrMineral)
            {
                console.log("[ERROR]: BehaviorHarvestEnergy can't find an active source");
                return;
            }

            if (creep.memory.behaviorMemory.state === "HARVEST")
            {
                if (_.sum(creep.carry) < creep.carryCapacity)
                {
                    if (creep.harvest(targetSourceOrMineral) === ERR_NOT_IN_RANGE)
                        creep.moveTo(targetSourceOrMineral, moveToOptions(this));
                }
                else
                {
                    creep.memory.behaviorMemory.state = "STORE";
                }
            }

            if (creep.memory.behaviorMemory.state === "STORE")
            {
                if (!creep.memory.behaviorMemory.targetContainer)
                {
                    const targetContainer = creep.pos.findClosestByPath(roomContainersAndStorage(creep.room));
                    if(targetContainer)
                        creep.memory.behaviorMemory.targetContainer = targetContainer.id;
                    else
                    {
                        console.log(Game.time + " [" + creep.room.name + "][WARN] tried to store energy in a container, but there is none!");
                        delete creep.memory.goal;
                        delete creep.memory.behaviorMemory;
                        return;
                    }
                }

                return BehaviorStoreResource.run(creep, creep.memory.behaviorMemory.targetContainer);
            }
        }
    };

/**
 * stores held resources in a container or storage.  Requires a target.
 * @type {{name: string; run(creep: Creep, targetContainerOrStorageId: string): undefined}}
 */
export const BehaviorStoreResource: Behavior =
    {
        name:"BehaviorStoreResource",
        run(creep: Creep, targetContainerOrStorageId: string)
        {
            const container: StructureContainer | StructureStorage = <StructureContainer | StructureStorage>Game.getObjectById(targetContainerOrStorageId);
            if (!container)
            {
                console.log(Game.time + "[" + creep.room.name + "][ERROR] Could not find target container or storage!");
                return;
            }

            const targetResource: ResourceConstant = <ResourceConstant>_.max(_.keys(creep.carry), resource =>
            {
                return (creep.carry as any)[resource];
            });

            const transferResult = creep.transfer(container, targetResource, creep.carry[targetResource]);
            switch (transferResult)
            {
                //too far away from target
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(container, moveToOptions(this));
                    break;

                //success
                case OK:
                    break;

                //target is full
                case ERR_FULL:
                    if( (targetResource !== RESOURCE_ENERGY) && creep.room.storage)
                    {
                        //if we are carrying something that isn't energy and there is a storage structure, fall back to that instead.
                        creep.memory.goal!.targetId = creep.room.storage.id;
                        break;
                    }
                    else
                    {
                        //otherwise, clear the goal.
                        delete creep.memory.behaviorMemory;
                        delete creep.memory.goal;
                        return;
                    }

                //we dont actually have the resources to perform the transfer
                case ERR_NOT_ENOUGH_RESOURCES:
                    delete creep.memory.behaviorMemory;
                    delete creep.memory.goal;
                    return;

                default:
                    console.log("Unhandled transfer result: " + transferResult);
                    break;
            }

            if (creep.carry.energy === 0)
            {
                delete creep.memory.behaviorMemory;
                delete creep.memory.goal;
            }
        }
    };

/**
 * transfers energy the creep is holding to a spawn or spawn extension
 * @type {{name: string; run(creep: Creep, targetStructureId: string): undefined}}
 */
export const BehaviorSupplySpawnOrExtension: Behavior =
    {
        name: "BehaviorSupplySpawnOrExtension",
        run(creep: Creep, targetStructureId: string)
        {
            const targetStructure = <StructureSpawn | StructureExtension>Game.getObjectById(targetStructureId);

            if (!targetStructure)
            {
                console.log("[ERROR]: BehaviorSupplySpawnOrExtension can't find target (" + targetStructureId + ")");
                return;
            }

            const transferResult: ScreepsReturnCode =
                creep.transfer(targetStructure,
                    RESOURCE_ENERGY,
                    Math.min(creep.carry.energy, targetStructure!.energyCapacity - targetStructure!.energy));

            switch (transferResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetStructure, moveToOptions(this));

                    if (creep.carry.energy === 0)
                        delete creep.memory.goal;
                    break;

                case ERR_NOT_ENOUGH_RESOURCES:
                case ERR_FULL:
                    delete creep.memory.goal;
                    break;

                case OK:
                    break;

                default:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
            }
        }
    };

/**
 * transfers energy the creep is holding to a terminal, but only bringing the balance up to Memory.globalSettings.desiredTerminalEnergyBalance
 * @type {{name: string; run(creep: Creep, targetStructureId: string): undefined}}
 */
export const BehaviorSupplyTerminal: Behavior =
    {
        name: "BehaviorSupplyTerminal",
        run(creep: Creep)
        {
            const terminal = creep.room.terminal;

            if (!terminal)
            {
                console.log("[ERROR]: BehaviorSupplyTerminal can't find terminal");
                delete creep.memory.goal;
                return;
            }

            if(terminal.store.energy > Memory.globalSettings.desiredTerminalEnergyBalance)
                delete creep.memory.goal;

            const transferResult: ScreepsReturnCode =
                creep.transfer(terminal,
                    RESOURCE_ENERGY,
                    Math.min(creep.carry.energy, Memory.globalSettings.desiredTerminalEnergyBalance - terminal.store.energy));

            switch (transferResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(terminal, moveToOptions(this));

                    if (creep.carry.energy === 0)
                        delete creep.memory.goal;
                    break;

                case ERR_NOT_ENOUGH_RESOURCES:
                case ERR_FULL:
                case OK:
                    delete creep.memory.goal;
                    break;

                default:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
            }
        }
    };

/**
 * supplies a lab sith the specified mineral
 * @type {{name: string; run(creep: Creep, targetMineral: string): undefined}}
 */
export const BehaviorSupplyLabWithMineral: Behavior =
    {
        name: "BehaviorSupplyLabWithMineral",
        run(creep: Creep, targetMineral: ResourceConstant)
        {
            const targetLab: StructureLab = <StructureLab>_.find(creep.room.lookForAt(LOOK_STRUCTURES, creep.memory.goal!.pos.x, creep.memory.goal!.pos.y), s => s.structureType === STRUCTURE_LAB);
            if(!targetLab)
            {
                if(creep.room.memory.logSettings.debug)
                    console.log(Game.time + " [" + creep.room.name + "][ERROR]: BehaviorSupplyLab can't find a lab at the goal position");

                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }
            if(!targetMineral)
            {
                if(creep.room.memory.logSettings.debug)
                    console.log(Game.time + " [" + creep.room.name + "][ERROR]: BehaviorSupplyLab was not given a target mineral");

                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            if(!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {holdingTargetMineral: false};

            if(!creep.memory.behaviorMemory.holdingTargetMineral)
            {
                //obtain the mineral to deposit
                if(creep.carry[targetMineral])
                {
                    //we already have it.  skip this step.
                    creep.memory.behaviorMemory.holdingTargetMineral = true;
                    return;
                }

                const structuresWithTargetMineral = roomContainersAndStorage(creep.room).filter(s => s.store[targetMineral]);
                const closestStructureWithTargetMIneral = creep.pos.findClosestByPath(structuresWithTargetMineral);

                if(closestStructureWithTargetMIneral)
                {
                    //withdraw minerals

                    //maximum amount we could withdraw
                    let transferSize = Math.min(creep.carryCapacity - _.sum(creep.carry), closestStructureWithTargetMIneral.store[targetMineral]!);

                    //try to withdraw an amount that would bring the lab to a multiple of 15
                    if( ((targetLab.mineralAmount + transferSize) % 15) !== 0 )
                        transferSize -= targetLab.mineralAmount % 15;

                    const withdrawResult: ScreepsReturnCode =
                        creep.withdraw(closestStructureWithTargetMIneral, targetMineral, );

                    switch (withdrawResult)
                    {
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(closestStructureWithTargetMIneral, moveToOptions(this));
                            break;

                        case ERR_NOT_ENOUGH_RESOURCES:
                        case ERR_FULL:
                            delete creep.memory.goal;
                            delete creep.memory.behaviorMemory;
                            break;

                        case OK:
                            creep.memory.behaviorMemory.holdingTargetMineral = true;
                            break;

                        default:
                            console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.withdraw error: " + withdrawResult.toString());
                    }
                }
            }
            else
            {
                //deposit held mineral into lab

                //maximum possible transfer
                let transferSize = Math.min(creep.carry[targetMineral]!, targetLab.mineralCapacity - targetLab.mineralAmount);

                //only transfer an amount that would bring the lab to a multiple of 15
                if( ((targetLab.mineralAmount + transferSize) % 15) !== 0 )
                    transferSize -= targetLab.mineralAmount % 15;

                const transferResult: ScreepsReturnCode = creep.transfer(targetLab, targetMineral, transferSize);

                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetLab, moveToOptions(this));

                        if (!creep.carry[targetMineral])
                        {
                            delete creep.memory.goal;
                            delete creep.memory.behaviorMemory;
                        }

                        break;

                    case ERR_NOT_ENOUGH_RESOURCES:
                    case ERR_FULL:
                        delete creep.memory.goal;
                        delete creep.memory.behaviorMemory;
                        break;

                    case OK:
                    case ERR_INVALID_ARGS:
                        if ((creep.carry[targetMineral] || 0) < 15)
                        {
                            delete creep.memory.goal;
                            delete creep.memory.behaviorMemory;
                        }

                        break;

                    default:
                        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled creep.transfer error: " + transferResult.toString());
                }
            }
        }
    };

/**
 * performs construction on existing sites
 * @type {{name: string; run(creep: Creep, constructionSiteId: string): undefined}}
 */
export const BehaviorConstruct: Behavior =
    {
        name: "BehaviorConstruct",
        run(creep: Creep, constructionSiteId: string)
        {
            const targetSite = <ConstructionSite>Game.getObjectById(constructionSiteId);

            if (!targetSite)
            {
                if(creep.room.memory.logSettings.debug)
                    console.log("[ERROR]: BehaviorConstructSite can't find target (" + constructionSiteId + ")");
                delete creep.memory.goal;
                return;
            }

            if (creep.build(targetSite) === ERR_NOT_IN_RANGE)
                creep.moveTo(targetSite, moveToOptions(this));

            if (creep.carry.energy === 0 || targetSite.progress === targetSite.progressTotal)
                delete creep.memory.goal;
        }
    };

/**
 * repairs the target structure
 * @type {{name: string; run(creep: Creep, targetStructureId: string): undefined}}
 */
export const BehaviorRepair: Behavior =
    {
        name: "BehaviorRepair",
        run(creep: Creep, targetStructureId: string)
        {
            const targetStructure = <Structure>Game.getObjectById(targetStructureId);

            if (!targetStructure)
            {
                console.log("[ERROR]: BehaviorRepair can't find target (" + targetStructureId + ")");
                return;
            }

            if (creep.repair(targetStructure) === ERR_NOT_IN_RANGE)
                creep.moveTo(targetStructure, moveToOptions(this, 3));

            if (creep.carry.energy === 0 || targetStructure.hits === targetStructure.hitsMax)
                delete creep.memory.goal;
        }
    };

/**
 * collects resources from the target container or tombstone
 * @type {{name: string; run(creep: Creep, targetContainerId: string): undefined}}
 */
export const BehaviorCollectFromContainer: Behavior =
    {
        name: "BehaviorCollectFromContainer",
        run(creep: Creep, targetContainerId: string)
        {
            const targetContainer = <StructureContainer | Tombstone>Game.getObjectById(targetContainerId);

            if (!targetContainer)
            {
                console.log(Game.time + " [" + creep.room.name + "][ERROR]: " + this.name + " can't find target (" + targetContainerId + ")");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            const targetResource: ResourceConstant = <ResourceConstant>_.max(_.keys(targetContainer.store), resource =>
            {
                //if the room has no storage, don't consider resources other than energy
                if(resource === RESOURCE_ENERGY || creep.room.storage)
                    return (targetContainer.store as any)[resource];
                else
                    return -1;
            });

            //@ts-ignore permit the following call even though Tombstone is not in the ts starter's list of valid structures
            const withdrawResult = creep.withdraw(targetContainer, targetResource);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_INVALID_ARGS: //this one can happen if the structure no longer has the target resource by the time we go to withdraw it
                    if(!<Tombstone>targetContainer)
                        console.log(Game.time + " [" + creep.room.name + "] " + this.name + ": invalid args(" + targetContainer + ", " + targetResource + ")");
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetContainer, moveToOptions(this));
                    break;
                default:
                    console.log("[ERROR]: unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (_.sum(targetContainer.store) === 0 || _.sum(creep.carry) === creep.carryCapacity || (!creep.room.storage && (targetContainer.store as any)[RESOURCE_ENERGY] === 0))
                delete creep.memory.goal;
        }
    };

/**
 * collects resources from the target container
 * @type {{name: string; run(creep: Creep, targetLabId: string): undefined}}
 */
export const BehaviorCollectFromLab: Behavior =
    {
        name: "BehaviorCollectFromLab",
        run(creep: Creep, targetLabId: string)
        {
            const targetLab = <StructureLab>Game.getObjectById(targetLabId);

            if (!targetLabId)
            {
                console.log("[ERROR]: BehaviorCollectFromLab can't find target (" + targetLabId + ", " + creep.memory!.goal!.targetId + ")");
                return;
            }

            //dont withdraw from an empty lab
            if(targetLab.mineralType === null)
            {
                delete creep.memory.goal;
                return;
            }

            const withdrawResult = creep.withdraw(targetLab, targetLab.mineralType);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(targetLab, moveToOptions(this));
                    break;
                default:
                    console.log("[ERROR]: unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (targetLab.mineralAmount === 0 || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * collects energy from the room's storage structure
 * @type {{name: string; run(creep: Creep, targetContainerId: string): undefined}}
 */
export const BehaviorCollectEnergyFromStorage: Behavior =
    {
        name: "BehaviorCollectEnergyFromStorage",
        run(creep: Creep)
        {
            const storage = creep.room.storage;

            if (!storage)
            {
                console.log("[ERROR]: BehaviorCollectEnergyFromStorage can't find storage!  goal removed!");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            const withdrawResult = creep.withdraw(storage, RESOURCE_ENERGY);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(storage, moveToOptions(this));
                    break;
                case ERR_INVALID_ARGS:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + ": invalid args(" + storage + ", " + RESOURCE_ENERGY + ")");
                    break;
                default:
                    console.log(Game.time + " [" + creep.room.name + "] [ERROR]: unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (!storage.store.energy || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * collects energy from the room's terminal, making sure to leave Memory.globalSettings.desiredTerminalEnergyBalance inside for performing transactions
 * @type {{name: string; run(creep: Creep, targetContainerId: string): undefined}}
 */
export const BehaviorCollectEnergyFromTerminal: Behavior =
    {
        name: "BehaviorCollectEnergyFromTerminal",
        run(creep: Creep)
        {
            const terminal = creep.room.terminal;

            if (!terminal)
            {
                console.log("[ERROR]: BehaviorCollectEnergyFromTerminal can't find terminal!  goal removed!");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            const withdrawAmount = (terminal.store.energy - Memory.globalSettings.desiredTerminalEnergyBalance);
            const withdrawResult = creep.withdraw(terminal, RESOURCE_ENERGY, withdrawAmount);
            switch (withdrawResult)
            {
                case OK:
                case ERR_FULL:
                case ERR_NOT_ENOUGH_RESOURCES:
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(terminal, moveToOptions(this));
                    break;
                case ERR_INVALID_ARGS:
                    console.log(Game.time + " [" + creep.room.name + "] " + this.name + ": invalid args(" + terminal + ", " + RESOURCE_ENERGY + ", " + withdrawAmount + ")");
                    break;
                default:
                    console.log("[ERROR]: unhandled withdraw result: " + withdrawResult);
                    break;
            }

            if (terminal.store.energy <= Memory.globalSettings.desiredTerminalEnergyBalance || _.sum(creep.carry) === creep.carryCapacity)
                delete creep.memory.goal;
        }
    };

/**
 * attacks the controller
 * @type {{}}
 */
export const BehaviorAttackController: Behavior =
    {
        name: "BehaviorAttackController",
        run(creep: Creep)
        {
            if (creep.room.controller)
            {
                const attackResult = creep.attackController(creep.room.controller);
                switch (attackResult)
                {
                    case OK:
                    case ERR_TIRED:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.controller, moveToOptions(this));
                        break;
                    default:
                        console.log(Game.time + " [" + creep.room.name + "] [WARN] unhandled attackController result " + attackResult);
                        break;
                }
            }
            else
            {
                console.log(Game.time + " [" + creep.room.name + "] no controller to attack!  Removing malformed goal");
                delete creep.memory.goal;
            }
        }
    };

/**
 * attacks the target creep
 * @type {{}}
 */
export const BehaviorAttackCreep: Behavior =
    {
        name: "BehaviorAttackCreep",
        run(creep: Creep, creepId: string)
        {
            const targetCreep = <Creep>Game.getObjectById(creepId);
            if (targetCreep)
            {
                const attackResult = creep.attack(targetCreep);
                switch (attackResult)
                {
                    case OK:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetCreep, moveToOptions(this));
                        break;
                    default:
                        console.log(Game.time + " [" + creep.room.name + "] [WARN] unhandled attack result.");
                        break;
                }
            }
            else
            {
                console.log(Game.time + " [" + creep.room.name + "] could not find target creep to attack.");
                delete creep.memory.goal;
            }
        }
    };

/**
 * claims the room
 * @type {{}}
 */
export const BehaviorClaimRoom: Behavior =
    {
        name: "BehaviorClaimRoom",
        run(creep: Creep)
        {
            if (creep.room.controller)
            {
                const claimResult = creep.claimController(creep.room.controller);
                switch (claimResult)
                {
                    case OK:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.controller, moveToOptions(this));
                        break;
                    case ERR_INVALID_TARGET:
                        delete creep.memory.goal;
                        break;
                    default:
                        console.log(Game.time + " [" + creep.room.name + "] [WARN] unhandled claimController result.");
                        break;
                }
            }
            else
            {
                console.log(Game.time + " [" + creep.room.name + "] no controller to attack!  Removing malformed goal");
                delete creep.memory.goal;
            }
        }
    };

/**
 * goes to the flag, registers itself with said flag, and then waits there, without removing the goal,
 * until an associated flag order tells the creep what to do next.
 */
export const BehaviorMuster: Behavior =
    {
        name: "BehaviorMuster",
        run(creep: Creep, targetFlagName: string)
        {
            if (!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {};

            const flag: Flag = Game.flags[targetFlagName];
            if (!flag)
            {
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            if (!creep.memory.behaviorMemory.arrivedAtRallyPoint)
            {
                if (creep.pos.getRangeTo(Game.flags[targetFlagName]) > 2)
                {
                    creep.moveTo(Game.flags[targetFlagName], moveToOptions(this));
                }
                else
                {
                    creep.memory.behaviorMemory = {arrivedAtRallyPoint: true};

                    if(!flag.memory.flagCreeps)
                        flag.memory.flagCreeps = [];

                    flag.memory.flagCreeps.push(creep.name);
                }
            }
        }
    };

/**
 * sends the creep to the given position
 * @type {{name: string; run(creep: Creep): void}}
 */
export const BehaviorGoToPosition: Behavior =
    {
        name: "BehaviorGoToPosition",
        run(creep: Creep)
        {
            //extracting figures because what is stored in memory isnt an entirely valid roomPosition object
            creep.moveTo(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y, moveToOptions(this));

            if(!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {};

            //if moved into another room, go towards the center so we dont blink back and then clear the goal
            if (creep.pos.roomName !== creep.memory.goal!.pos.roomName)
            {
                if (!creep.memory.behaviorMemory.movedFromExit)
                {
                    creep.moveTo(25, 25, moveToOptions(this));
                    creep.memory.behaviorMemory.movedFromExit = true;
                }
                else
                {
                    delete creep.memory.goal;
                    delete creep.memory.behaviorMemory;
                }
            }
            else if (creep.pos.isEqualTo(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y))
            {
                //if at the target, clear goal
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
            }
        }
    };

/**
 * sends the creep to the given room
 * @type {{name: string; run(creep: Creep): void}}
 */
export const BehaviorGoToRoom: Behavior =
    {
        name: "BehaviorGoToRoom",
        run(creep: Creep, roomName: string)
        {
            if(creep.room.name === roomName)
            {
                //we are in the room!
                if(creep.pos.x === 0 ||
                   creep.pos.y === 0 ||
                   creep.pos.x === 49 ||
                   creep.pos.y === 49)
                {
                    creep.moveTo(25, 25, moveToOptions(this)); //we are on an exit: move towards the center of the room
                    return;
                }
                else
                {
                    //goal complete
                    delete creep.memory.goal;
                    delete creep.memory.behaviorMemory;
                    return;
                }
            }
            else
            {
                //find a route to the target room
                creep.moveTo(new RoomPosition(25,25,roomName));
            }
        }
    };

/**
 * picks up a dropped resource pile
 * @type {{name: string; run(creep: Creep, targetId: string): void}}
 */
export const BehaviorCleanupDroppedResources: Behavior =
    {
        name: "BehaviorCleanupDroppedResources",
        run(creep: Creep, targetId: string)
        {
            const target = <Resource>Game.getObjectById(targetId);
            const pickupResult = creep.pickup(target);
            switch (pickupResult)
            {
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(target, moveToOptions(this));
                    break;
                case ERR_FULL:
                case OK:
                case ERR_INVALID_TARGET:
                    delete creep.memory.goal;
                    break;
                default:
                    console.log("Unknown pickup result: " + pickupResult);
                    break;
            }
        }
    };

/**
 * responsible for transporting minerals from one room to another.  This takes a flag, and uses the following from that flag:
 * pos: the creep looks for a building at the flag's position to store the minerals into
 * target: the creep uses the flag's target as the mineral to transport from the original room
 * the creep will collect the target mineral from its own room and carry as much as it can to the target lab.
 * If that lab is full, the creep stores them in the room's storage structure instead, if it has one.
 * @type {{name: string; run(creep: Creep, requestingFlagName: string): void}}
 */
export const BehaviorTransportMinerals: Behavior =
    {
        name: "BehaviorTransportMinerals",
        run(creep: Creep, requestingFlagName: string)
        {
            if(!creep.memory.spawnedFromRoom)
            {
                console.log(Game.time + " [WARN] " + this.name + " on creep " + creep.name + " in room " + creep.room.name + " does not know what room it came from!");
                return;
            }

            const requestingFlag = Game.flags[requestingFlagName];
            if(!requestingFlag)
            {
                console.log(Game.time + " [" + creep.room.name + "] a transport had it's destination flag removed.  Releasing for general service");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
            }

            if(!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {state:"COLLECTING"};

            const targetMineral:ResourceConstant = <ResourceConstant>requestingFlag.memory.target;
            if(!targetMineral)
            {
                console.log(Game.time + " [" + creep.room.name + "] invalid target mineral: " + requestingFlag.memory.target);
                return;
            }

            //fill up with minerals
            if(creep.memory.behaviorMemory.state === "COLLECTING")
            {
                if(creep.room.name !== creep.memory.spawnedFromRoom)
                {
                    //if not in the room we were spawned from, go there
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.spawnedFromRoom), moveToOptions(this));
                }
                else
                {
                    //if in the room, collect minerals
                    const structuresWithTargetMineral: (StructureContainer | StructureStorage)[] =
                        roomContainersAndStorage(creep.room).filter(structure => (structure.store as any)[targetMineral]);
                    const targetStructure = creep.pos.findClosestByPath(structuresWithTargetMineral);

                    if(!targetStructure)
                    {
                        creep.memory.behaviorMemory.state = "TRANSPORTING";
                        return;
                    }

                    const withdrawResult = creep.withdraw(targetStructure, targetMineral);
                    switch (withdrawResult)
                    {
                        case OK:
                        case ERR_FULL:
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(targetStructure, moveToOptions(this));
                            break;
                        default:
                            console.log("[ERROR]: unhandled withdraw result: " + withdrawResult);
                            break;
                    }

                    if (_.sum(creep.carry) === creep.carryCapacity)
                        creep.memory.behaviorMemory.state = "TRANSPORTING";
                }
            }

            if (creep.memory.behaviorMemory.state === "TRANSPORTING")
            {
                //transport minerals
                const targetLab: StructureLab = <StructureLab>_.find(requestingFlag.pos.lookFor(LOOK_STRUCTURES), s => s.structureType === STRUCTURE_LAB);

                let targetStructure: StructureLab | StructureStorage;
                let transferAmount: number;

                if(targetLab.mineralAmount === targetLab.mineralCapacity && creep.room.storage)
                {
                    targetStructure = (creep.room.storage);
                    transferAmount = Math.min( (creep.carry[targetMineral] || 0), creep.room.storage.storeCapacity - _.sum(creep.room.storage.store))
                }
                else
                {
                    targetStructure = targetLab;
                    transferAmount = Math.min( (creep.carry[targetMineral] || 0), targetLab.mineralCapacity - targetLab.mineralAmount);

                    //since this is a lab, ensure a multiple of 15
                    if(((targetLab.mineralAmount + transferAmount) % 15) > 0)
                        transferAmount -= ((targetLab.mineralAmount + transferAmount) % 15);
                }

                if(!targetStructure)
                {
                    if (creep.room.memory.logSettings.debug)
                        console.log(Game.time + " [" + creep.room.name + "] nowhere to store transported minerals!");
                    return;
                }

                const transferResult = creep.transfer(targetStructure, targetMineral, transferAmount);
                switch (transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(targetStructure, moveToOptions(this));
                        break;

                    case OK:
                    case ERR_NOT_ENOUGH_RESOURCES:
                        if( (creep.carry[targetMineral] || 0) < 15)
                            creep.memory.behaviorMemory.state = "COLLECTING";
                        break;

                    case ERR_FULL:
                        break;

                    case ERR_INVALID_ARGS:
                        console.log(this.name + " used invalid args!", targetStructure, targetMineral, transferAmount);
                        // creep.memory.behaviorMemory.state = "COLLECTING";
                        break;

                    default:
                        console.log(this.name + " Unhandled transfer result: " + transferResult);
                        break;
                }
            }

        }
    };

/**
 * sells value units of resource resource at whatever the current market price is.
 * If there are open buy orders, this makes a deal with them.  If not, this undercuts the price of an existing sell order.
 * @type {{name: string; run(creep: Creep, resource: ResourceConstant, value: number): undefined}}
 */
export const BehaviorSellResourceAtAnyPrice: Behavior =
    {
        name:"BehaviorSellResourceAtAnyPrice",
        run(creep: Creep, resource:ResourceConstant, value:number)
        {
            const terminal = creep.room.terminal;

            if(!terminal)
            {
                console.log(Game.time + " [" + creep.room.name + "] " + this.name + " can't find a terminal!");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            if(!creep.room.memory.sellAtAnyPrice || creep.room.memory.sellAtAnyPrice.length === 0 || creep.room.memory.sellAtAnyPrice[0].resource !== resource)
            {
                console.log(Game.time + " [" + creep.room.name + "] " + this.name + " trade was invalidated");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
                return;
            }

            const amountStillNeeded = value - (terminal.store[resource] || 0);

            const availableTerminalCapacity = (terminal.storeCapacity - _.sum(terminal.store));

            if( availableTerminalCapacity < amountStillNeeded )
            {
                creep.room.memory.sellAtAnyPrice![0].amount = availableTerminalCapacity;
                creep.memory.goal!.value = (terminal.store[resource] || 0) + availableTerminalCapacity;
                console.log(Game.time + " [" + creep.room.name + "] shrank a sellAtAnyPrice request because the terminal did not have room");
            }

            if(creep.room.memory.logSettings.market)
                console.log(Game.time + " [" + creep.room.name + "] selling " + resource + " at any price. (" + amountStillNeeded + " remaining to transport)");

            if(amountStillNeeded <= 0 )
            {
                //terminal is ready to trade
                const buyOrdersForResource = Game.market.getAllOrders({resourceType: resource, type: ORDER_BUY});
                if(!buyOrdersForResource || buyOrdersForResource.length === 0)
                {
                    console.log(Game.time + " [" + creep.room.name + "] no buy orders for " + resource + ". Creating a sell order instead.");
                    const existingSellOrders = Game.market.getAllOrders({resourceType: resource, type:ORDER_SELL});
                    existingSellOrders.sort(o => o.price);
                    if(existingSellOrders.length === 0)
                    {
                        console.log(Game.time + " [" + creep.room.name + "] couldn't do that either: there are no sell orders to compare it to!");
                        return;
                    }
                    const sellPrice = existingSellOrders[existingSellOrders.length - 1].price;
                    Game.market.createOrder(ORDER_SELL, resource, sellPrice -0.001, value, creep.room.name);
                    creep.room.memory.sellAtAnyPrice!.splice(0, 1);

                    delete creep.memory.goal;
                    delete creep.memory.behaviorMemory;
                    return;
                }

                buyOrdersForResource.sort(o => o.price);
                const tradeAmount = Math.min(value, buyOrdersForResource[0].remainingAmount);
                const dealResult = Game.market.deal(buyOrdersForResource[0].id, tradeAmount, creep.room.name);
                switch(dealResult)
                {
                    case OK:
                        creep.room.memory.sellAtAnyPrice![0].amount -= tradeAmount;
                        if(creep.room.memory.sellAtAnyPrice![0].amount <= 0)
                            creep.room.memory.sellAtAnyPrice!.splice(0, 1);
                        console.log("Completed sellAtAnyPrice for " + resource);
                        delete creep.memory.goal;
                        delete creep.memory.behaviorMemory;
                        break;
                    case ERR_TIRED:
                        break;
                    case ERR_NOT_ENOUGH_RESOURCES:
                        console.log("insufficient resources to complete trade in " + creep.room.name + "!");
                        break;
                    default:
                        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled deal result " + dealResult);
                        break;
                }
            }
            else if(creep.carry[resource])
            {
                //put held resource into the terminal
                const transferResult = creep.transfer(terminal, resource, Math.min(creep.carry[resource]!, amountStillNeeded));
                switch(transferResult)
                {
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(terminal, moveToOptions(this));
                        break;

                    case OK:
                    case ERR_NOT_ENOUGH_RESOURCES:
                    case ERR_FULL:
                        break;

                    default:
                        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " Unhandled transfer result: " + transferResult);
                        break;
                }
            }
            else if (creep.room.storage && creep.room.storage.store[resource])
            {
                //collect resource from storage
                const withdrawResult = creep.withdraw(creep.room.storage, resource, Math.min((creep.carryCapacity - _.sum(creep.carry)), amountStillNeeded));
                switch (withdrawResult)
                {
                    case OK:
                    case ERR_FULL:
                    case ERR_NOT_ENOUGH_RESOURCES:
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.storage, moveToOptions(this));
                        break;
                    default:
                        console.log(Game.time + " [" + creep.room.name + " ][ERROR]: " + this.name + " unhandled withdraw result: " + withdrawResult);
                        break;
                }
            }
            else
            {
                //resource unavailable
                console.log(Game.time + " [" + creep.room.name + "] " + this.name + " can't find any stored " + resource + " to trade with!");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
            }
        }
    };

/**
 * transfers units of resource resource to the destination room
 * @type {{name: string; run(creep: Creep, resource: ResourceConstant, value: number): undefined}}
 */
export const BehaviorTransferResourceToRoom: Behavior =
  {
    name:"BehaviorTransferResourceToRoom",
    run(creep: Creep, target:string, value:number)
    {
      const terminal = creep.room.terminal;

      const targetParts: string[] = target.split("::");

      const resource = targetParts[0];
      const targetRoom = targetParts[1];

      if(!resource || !targetRoom)
      {
        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " was cancelled because of an invalid target string.  Should be format resource::roomName")
        delete creep.memory.goal;
        delete creep.memory.behaviorMemory;
      }

      if(!terminal)
      {
        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " can't find a terminal!");
        delete creep.memory.goal;
        delete creep.memory.behaviorMemory;
        return;
      }

      if(!creep.room.memory.transferResources || creep.room.memory.transferResources.length === 0 || creep.room.memory.transferResources[0].resource !== resource)
      {
        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " transfer was invalidated");
        delete creep.memory.goal;
        delete creep.memory.behaviorMemory;
        return;
      }

      const amountStillNeeded = value - (terminal.store[resource] || 0);

      const availableTerminalCapacity = (terminal.storeCapacity - _.sum(terminal.store));

      if( availableTerminalCapacity < amountStillNeeded )
      {
        creep.room.memory.transferResources![0].amount = availableTerminalCapacity;
        creep.memory.goal!.value = (terminal.store[resource] || 0) + availableTerminalCapacity;
        console.log(Game.time + " [" + creep.room.name + "] shrank a transferResources request because the terminal did not have room");
      }

      if(creep.room.memory.logSettings.market)
        console.log(Game.time + " [" + creep.room.name + "] transferring " + resource + " to room " + targetRoom + " (" + amountStillNeeded + " remaining to transport)");

      if(amountStillNeeded <= 0 )
      {
        //terminal is ready to transfer
        const transferResult = terminal.send(resource, value, targetRoom, "Transfer from " + creep.room.name);

        switch(transferResult)
        {
          case OK:
            creep.room.memory.transferResources!.splice(0, 1);
            console.log("Completed transfer of " + resource + " to room " + targetRoom);
            delete creep.memory.goal;
            delete creep.memory.behaviorMemory;
            break;
          case ERR_TIRED:
            break;
          case ERR_NOT_ENOUGH_RESOURCES:
            console.log("insufficient resources to complete transfer in " + creep.room.name + "!");
            break;
          default:
            console.log(Game.time + " [" + creep.room.name + "] " + this.name + " unhandled transfer result " + transferResult);
            break;
        }
      }
      else if(creep.carry[resource])
      {
        //put held resource into the terminal
        const transferResult = creep.transfer(terminal, resource, Math.min(creep.carry[resource]!, amountStillNeeded));
        switch(transferResult)
        {
          case ERR_NOT_IN_RANGE:
            creep.moveTo(terminal, moveToOptions(this));
            break;

          case OK:
          case ERR_NOT_ENOUGH_RESOURCES:
          case ERR_FULL:
            break;

          default:
            console.log(Game.time + " [" + creep.room.name + "] " + this.name + " Unhandled transfer result: " + transferResult);
            break;
        }
      }
      else if (creep.room.storage && creep.room.storage.store[resource])
      {
        //collect resource from storage
        const withdrawResult = creep.withdraw(creep.room.storage, resource, Math.min((creep.carryCapacity - _.sum(creep.carry)), amountStillNeeded));
        switch (withdrawResult)
        {
          case OK:
          case ERR_FULL:
          case ERR_NOT_ENOUGH_RESOURCES:
            break;
          case ERR_NOT_IN_RANGE:
            creep.moveTo(creep.room.storage, moveToOptions(this));
            break;
          default:
            console.log(Game.time + " [" + creep.room.name + " ][ERROR]: " + this.name + " unhandled withdraw result: " + withdrawResult);
            break;
        }
      }
      else
      {
        //resource unavailable
        console.log(Game.time + " [" + creep.room.name + "] " + this.name + " can't find any stored " + resource + " to transfer!");
        delete creep.memory.goal;
        delete creep.memory.behaviorMemory;
      }
    }
  };

/**
 * special behavior that is run on creeps that have no goal assigned.
 * no goal should ever use this behavior!
 * @type {{name: string; run(creep: Creep): void}}
 */
export const BehaviorIdle: Behavior =
    {
        name:"BehaviorIdle",
        run(creep: Creep)
        {
            //fallback code to try and prevent creeps from sitting around with inventories full of minerals when storage is down
            if(_.sum(creep.carry) > creep.carry.energy)
            {
                //idle creep is holding non-energy resources.  this is probably a bad sign...
                if(creep.room.storage)
                {
                    console.log(Game.time + " [" + creep.room.name + "][WARN] an idle creep is holding minerals even though a storage structure exists!  This probably means something is wrong with goal selection and/or assignment");
                }
                else
                {
                    //there is no storage.  Look for a container to use instead
                    const containers = roomContainers(creep.room).filter(container => (container.storeCapacity - _.sum(container.store)) > 200);
                    if(containers.length === 0)
                    {
                        console.log(Game.time + " [" + creep.room.name + "][WARN] creeps have nowhere to store minerals!");
                    }
                    else
                    {
                        const target = containers.sort(container => container.storeCapacity - _.sum(container.store))[0];
                        creep.memory.goal =
                            {
                                name: "Store minerals in container",
                                pos: target.pos,
                                priority: PRIORITY_STORE_RESOURCE,
                                targetId: target.id,
                                behaviorName: BehaviorStoreResource.name,
                                partsRequested: [],
                            };
                        return;
                    }
                }
            }

            //track how long unit has been idle
            if(!creep.memory.idleSince)
                creep.memory.idleSince = Game.time;

            //reasons for a creep to suicide/recycle itself
            if( (creep.memory.specialized && (Game.time - creep.memory.idleSince) >= 100 ) || //specialized creeps that are idle for 100 ticks
                (_.every(creep.body, bpd => bpd.hits === 0 || bpd.type === MOVE)))    //creeps where all working parts are MOVE
            {
                    const roomSpawn = creep.room.find(FIND_MY_SPAWNS)[0];
                    if(!roomSpawn)
                    {
                        console.log(Game.time + " [" + creep.room.name + "] creep " + creep.name + " has committed suicide.");
                        creep.suicide();
                    }
                    else
                    {
                        creep.say("Recycling");
                        if (roomSpawn.recycleCreep(creep) === ERR_NOT_IN_RANGE)
                            creep.moveTo(roomSpawn, moveToOptions(this));
                    }
            }
            else
            {
                creep.say("ZZZ");
                const idleFlag = _.find(Game.flags, flag => (flag.pos.roomName === creep.room.name && flagSpecOf(flag) && flagSpecOf(flag)!.name === "MUSTER_IDLE"));
                if(idleFlag)
                    creep.moveTo(idleFlag, moveToOptions(this));
                else
                    creep.moveTo(idlePos(creep.room), moveToOptions(this));
            }
        }
    };

/**
 * responsible for transporting minerals from one room to another.  This takes a flag, and uses the following from that flag:
 * pos: the creep looks for a building at the flag's position to store the minerals into
 * target: the creep uses the flag's target as the mineral to transport from the original room
 * the creep will collect the target mineral from its own room and carry as much as it can to the target lab.
 * If that lab is full, the creep stores them in the room's storage structure instead, if it has one.
 * @type {{name: string; run(creep: Creep, requestingFlagName: string): void}}
 */
export const BehaviorTransportEnergy: Behavior =
    {
        name: "BehaviorTransportEnergy",
        run(creep: Creep)
        {
            if(!creep.memory.spawnedFromRoom)
            {
                console.log(Game.time + " [" + creep.room.name + "]" + this.name + " on creep " + creep.name + " in room " + creep.room.name + " does not know what room it came from!");
                return;
            }

            const requestingFlag = new RoomPosition(creep.memory.goal!.pos.x, creep.memory.goal!.pos.y, creep.memory.goal!.pos.roomName).lookFor(LOOK_FLAGS)[0];
            if(!requestingFlag)
            {
                console.log(Game.time + " [" + creep.room.name + "] a transport had it's destination flag removed.  Releasing for general service");
                delete creep.memory.goal;
                delete creep.memory.behaviorMemory;
            }

            if(!creep.memory.behaviorMemory)
                creep.memory.behaviorMemory = {state:"COLLECTING"};

            //fill up with energy
            if(creep.memory.behaviorMemory.state === "COLLECTING")
            {
                if(creep.room.name !== creep.memory.goal!.pos.roomName)
                {
                    //if not in the room the goal is in, go there
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.goal!.pos.roomName), moveToOptions(this));
                }
                else
                {
                    if (!creep.memory.behaviorMemory.containerIds)
                        creep.memory.behaviorMemory.containerIds = roomContainers(creep.room).map(c => c.id);

                    if(creep.memory.behaviorMemory.containerIds.length === 0)
                    {
                        console.log(Game.time + " [WARN] no containers present in remote harvesting room " + creep.room.name);
                        const idleFlag = _.find(Game.flags, flag => flag.pos.roomName === creep.room.name);
                        if(idleFlag)
                            creep.moveTo(idleFlag, moveToOptions(this));
                        else
                            creep.moveTo(25, 25, moveToOptions(this));
                    }

                    const containers: StructureContainer[] = creep.memory.behaviorMemory.containerIds.map(((id:string) => Game.getObjectById(id)));

                    const targetContainerIndex = creep.memory.behaviorMemory.containerIndex || 0;
                    const targetContainer = containers[targetContainerIndex];

                    const withdrawResult = creep.withdraw(targetContainer, RESOURCE_ENERGY);
                    switch (withdrawResult)
                    {
                        case OK:
                        case ERR_FULL:
                            creep.memory.behaviorMemory.containerIndex = (targetContainerIndex+1) % containers.length;
                            break;
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(targetContainer, moveToOptions(this));
                            break;
                        case ERR_INVALID_TARGET:
                            console.log(Game.time + " [WARN] " + this.name + "[" + creep.room.name + "] resetting container memory because one of the containers disappeared.");
                            delete creep.memory.behaviorMemory.containerIds;
                            delete creep.memory.behaviorMemory.containerIndex;
                            break;
                        default:
                            console.log(this.name + " [ERROR]: unhandled withdraw result: " + withdrawResult);
                            break;
                    }

                    if (_.sum(creep.carry) === creep.carryCapacity)
                        creep.memory.behaviorMemory.state = "TRANSPORTING";
                }
            }

            if (creep.memory.behaviorMemory.state === "TRANSPORTING")
            {
                if(creep.room.name !== creep.memory.spawnedFromRoom)
                {
                    creep.moveTo(new RoomPosition(25, 25, creep.memory.spawnedFromRoom), moveToOptions(this));
                }
                else
                {
                    //transport energy
                    if (!creep.room.storage)
                    {
                        if (creep.room.memory.logSettings.debug)
                            console.log(Game.time + " [" + creep.room.name + "] nowhere to store transported energy!");
                        return;
                    }

                    const transferResult = creep.transfer(creep.room.storage, RESOURCE_ENERGY);
                    switch (transferResult)
                    {
                        case ERR_NOT_IN_RANGE:
                            creep.moveTo(creep.room.storage, moveToOptions(this));
                            break;
                        case OK:
                        case ERR_NOT_ENOUGH_RESOURCES:
                            break;
                        case ERR_FULL:
                            break;
                        case ERR_INVALID_ARGS:
                            console.log(this.name + " used invalid args!", creep.room.storage, RESOURCE_ENERGY);
                            break;
                        default:
                            console.log(this.name + " Unhandled transfer result: " + transferResult);
                            break;
                    }

                    if(creep.carry.energy === 0)
                        creep.memory.behaviorMemory.state = "COLLECTING";
                }
            }
        }
    };

// type shim for nodejs' `require()` syntax
// for stricter node.js typings, remove this and install `@types/node`
declare const require: (module: string) => any;

// add your custom typings here

/**
 * represents some sort of action to be taken
 */
interface Goal
{
    /**
     * name of the goal to be taken
     */
    name : string;

    /**
     * body parts needed to carry out the goal
     */
    partsRequested : PartRequest[];

    /**
     * priority for this goal
     */
    priority : number;

    /**
     * name of the Behavior to be carried out by units carrying out this goal
     * goals with the same name are processed together, so it is important that
     * all goals with the same name and priority have the same REQUIREMENTS
     * It is, however, acceptable for their PREFERENCES to vary
     */
    behaviorName : string;

    /**
     * position this goal is to be performed at
     */
    pos : RoomPosition;

    /**
     * if specified, this is the target that should be passed to the behavior
     */
    targetId? : string;

    /**
     * if specified, this is the value that should be passed to the behavior
     */
    value? : number;

    /**
     * if specified, creep needs at least this much energy to perform the task
     */
    minimumEnergy? : number;

    /**
     * if specified, creep needs at least this much of any non-energy resource to perform the task
     */
    minimumMinerals? : number;

    /**
     * if specified, creep must have at least this much open capacity
     */
    minimumOpenCapacity? : number;

    /**
     * if specified, goal PREFERS a creep with at least this much energy
     */
    prefersEnergy? : number;

    /**
     * if specified, goal PREFERS a creep with at least this much open capacity
     */
    prefersOpenCapacity? : number;

    /**
     * if specified, maximum number of creeps that can work on this goal at once
     */
    maximumCreepCount? : number;

    /**
     * if no creeps could meet the requirements of this goal, puts in a request for a unit with the given body.
     */
    ifRequirementsUnmetSpawn? : string;
}

/**
 * defines how a creep will carry out the associated goal
 */
interface Behavior
{
    name : string,
    run(creep : Creep, targetId? : string, value? : number) : void;
}

/**
 * represents a request for some number of a specific body part
 */
interface PartRequest
{
    part:BodyPartConstant, //desired part
    min?:number,           //minimum required (default: 1)
    preferred?:number      //preferred amount

    //maximum of this part to be put on the goal.
    // Note that this is a "soft cap", meaning an assignment can put it over this number,
    // but no new assignments will be made if the cap has already been reached
    max?:number
}

/**
 * used for aggregate counting of creeps and their body parts
 */
interface CreepCounts
{
    creeps:number
    parts: CreepCount[];
}

interface CreepCount {part:BodyPartConstant, count:number}

/**
 * GoalProviders are responsible for providing a set of Goals.
 * Typically they are associated with some sort of object, and examine the object to determine whether or not
 * action should be taken.
 */
interface GoalProvider
{
    /**
     * examines the given object and returns an array of Goals based on that object.
     * Note that this array may be empty.
     */
    evaluate(target: any) : Goal[];
}

/**
 * specification for a flag.  Defines what a particular flag in the world means.
 */
interface FlagSpec
{
    /**
     * primary color of the flag
     */
    primary   : ColorConstant;

    /**
     * secondary color of the flag
     */
    secondary : ColorConstant;

    /**
     * name of this particular spec, NOT the name of the flag.
     */
    name?     : string;

    /**
     * if set, this GoalProvider is polled during goal evaluation
     * for example, a harvest flag indicates a source that needs harvesting, but it may not have energy right now
     * the GoalProvider is called for this flag to determine what should be done with it
     */
    goalProvider? : GoalProvider;

    /**
     * if set, this flag represents an Order to carry out some task
     * for example, a debug flag to wipe all creep assignments in the room
     */
    order? : FlagOrder;
}

/**
 * represents a command for overarching behavior
 */
interface FlagOrder
{
    run(flag : Flag) : void;
}

/**
 * represents x and y offsets from some position
 */
interface PositionOffset
{
    x: number,
    y: number
}

/**
 * defines how creep memory should be structured
 */
interface CreepMemory
{
    goal?: Goal,              //goal this creep is currently working on
    behaviorMemory?: any,     //typeless storage for whatever data a behavior may have to store
    specialized?: boolean,    //if true, this creep is specialized for a specific purpose and will be ignored when determining whether a room needs more units
    spawnedFromRoom?: string, //name of the room that spawned this creep
    idleSince?:number,        //first tick this creep became idle
}

/**
 * defines how room memory should be structured
 */
interface RoomMemory
{
    //how structures in the room should be repaired
    repairSettings:
        {
            maxDefenseHealth:number, //walls wont be repaired above this many hits
            wallRepairThreshold:0    //if set, storage must contain at least this much energy for walls to be repaired
        },

    //what kinds of logging should be done
    logSettings: LogSettings,

    //defines how spawning should behave in this room
    spawnSettings:
        {
            targetCreepCounts:CreepCounts,
        },

    //specifies how automatic construction should be performed
    constructionSettings:
        {
            forceTheseSettings: boolean, //if true, high level automation will never update these
            spawnExtensions: boolean,
            roadsBySpawnsAndExtensions: boolean,
            roadsByTowers: boolean,
            containersBySources: boolean,
            containersByExtractors: boolean,
            towers: boolean,
            extractors: boolean,
            storage: boolean,
        },

    //stores room construction metadata
    constructionMemory:
        {
            nextSpawnExtensionOffset?: PositionOffset,
            SpawnExtensionBuildDirection?: TOP | RIGHT | BOTTOM | LEFT,
        },

    //statistics
    statistics:
        {
            unspecializedIdleCreepTicks: number[], //number of unspecialized creeps that were idle in the past x ticks
            unspecializedCreepTicks: number[],     //number of unspecialized creeps in the past x ticks
            upgradesTicks: number[],               //number of upgrade calls in the past x ticks
            upgradesThisTick: number,              //number of upgrade calls THIS tick

            averages:
                {
                    creeps:   number, //average number of creeps per tick
                    idle:     number, //average number of idle creeps per tick
                    upgrades: number, //average number of upgrade operations per tick
                },

            roundedAverages:
                {
                    creeps:   number, //rounded average number of creeps per tick
                    idle:     number, //rounded average number of idle creeps per tick
                    upgrades: number, //rounded average number of upgrade operations per tick
                }
        },

    //automation metadata
    timeOfLastCreepCountAnalysis?:number, //so autoAdjustSpawnCounts knows when to run
    spawnCountAdjustment?:number,         //adjustment maintained by autoAdjustSpawnCounts
    remoteHarvestingInRoom?:boolean,      //if true, some forms of high level automation apply even if the room is unowned

    //trading
    sellAtAnyPrice?:{resource:ResourceConstant, amount:number}[],
    buyAtAnyPrice?:{resource:ResourceConstant, amount:number}[],
    transferResources?:{resource:ResourceConstant, amount:number, destination:string}[],

    //activity tracking
    lastActiveTick: number,
}


interface FlagMemory
{
    target?:string | null,              //target of this flag.  Exact meaning varies by flag, but typically this is the structure the flag is referring to
    value?:number,                      //value of this flag.   Exact meaning varies by flag, but typically this is used by flags that need to keep track of some numeric value
    orderState?:string,                 //state of this flag.   Exact meaning varies by flag, but typically this is used for the flag to keep track of which step of a complicated order it is on
    requestingCreepType?:string,        //the type of creep this flag would like to come assist.  In this way the flag can ask for help from nearby rooms, but also alter/cancel said request if something changes before help is sent.
    requestingCreepMemory?:CreepMemory, //if specified, memory to assign the newly created creep.  If not specified, a default goal will be created to send the creep to the room making the request.
    flagCreeps?:string[],               //names of creeps working with for this flag.  Exact meaning varies by flag, but typically this is either creeps that were spawned in response to a request from this flag or creeps which are rallying to this flag.
}

interface SpawnMemory
{
    unmetPreferences:string[];
    remoteSpawnRequests:RemoteSpawnRequest[];
}

interface RemoteSpawnRequest
{
    requestingFlagName:string,          //name of the flag desiring help
}

interface Math {sign(x:number):number} //add signature for a JS function that, oddly, TS doesn't recognize on its own

// how global memory is structured
interface Memory
{
    globalSettings:GlobalSettings,
    globalAutomationMemory?:GlobalAutomationMemory,
    creeps: {[index:string]:CreepMemory},
    flags:  {[index:string]:FlagMemory},
    rooms:  {[index:string]:RoomMemory},
    spawns: {[index:string]:SpawnMemory},
}

interface GlobalAutomationMemory
{
    handleOldOrders?: {orderId: string, lastAdjustedTime:number}[]
}

interface GlobalSettings
{
    //which kinds of high level automation to perform
    autoConstruct: boolean,
    autoRoomSettings: boolean,
    autoHarvestFlags: boolean,
    autoAdjustSpawnCounts: boolean,
    handleOldSellOrders: boolean,

    //number of ticks to keep track of statistics figures for.  once this is reached, old entries are pushed out to make room for new ones
    //this impacts both memory and performance if set too high
    statisticsTrackingHistoryLength: number,

    //terminals will try to have EXACTLY this much energy at all times, to be used for purchases.  Any excess is carried away.
    desiredTerminalEnergyBalance: number,

    //log settings to use on new rooms, or when flag order DEBUG_RESET_LOGGING is run.
    defaultLogSettings : LogSettings,

    //number of ticks to reuse any given path before calculating it again
    //lower values produce better movement, but have a severe performance impact
    moveToReusePathTicks: number,

    oldOrderThreshold: number,
}

//Tombstone definition missing from the starter I used
interface Tombstone
{
    pos: RoomPosition,
    room: Room,
    creep: Creep,
    deathTime: number,
    id: string,
    store: {[index:string]:number},
    ticksToDecay: number,
}

interface LogSettings
{
    availableCreepCount?:boolean,
    damagedStructureCount?:boolean,
    debug?:boolean,
    goalCount?:boolean,
    goalList?:boolean,
    goalListVerbose?:boolean,
    goalUnmetPreferences?:boolean,
    newAssignments?:boolean,
    spawning?:boolean,
    summary?:boolean,
    taskList?:boolean,
    statistics?:boolean,
    market?:boolean,
}

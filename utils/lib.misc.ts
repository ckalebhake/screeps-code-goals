import {flagSpecs} from "../flagSpecs";

/**
 * fetches the flag spec for a given flag
 * @param {Flag} flag
 * @returns {FlagSpec | null}
 */
export function flagSpecOf(flag: Flag): FlagSpec | null
{
    const specs: FlagSpec[] = flagSpecs.filter(spec => spec.primary === flag.color && spec.secondary === flag.secondaryColor);
    if (specs.length === 1)
        return specs[0];
    else if (specs.length === 0)
    {
        console.log("No spec found for flag " + JSON.stringify(flag));
        return null;
    }
    else
    {
        console.log("multiple flag specs found for flag " + JSON.stringify(flag));
        return null;
    }
}
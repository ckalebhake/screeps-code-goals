/**
 * returns all spawns and spawn extensions in the room
 *
 * @param room the room to search
 * @returns {*} an array containing all spawns and spawn extensions in the room
 */
import {flagSpecOf} from "./lib.misc";

export function roomSpawnsAndExtensions(room: Room) : (StructureSpawn | StructureExtension)[]
{
    return <(StructureSpawn | StructureExtension)[]>room.find(
        FIND_MY_STRUCTURES, {
          filter: function (structure)
          {
            return structure.structureType === STRUCTURE_SPAWN || structure.structureType === STRUCTURE_EXTENSION
          }
      }
    );
}

/**
 * returns all containers in the room
 *
 * @param room the room to search
 * @returns {*} an array containing all containers in the room
 */
export function roomContainers(room: Room) : StructureContainer[]
{
    return <StructureContainer[]>room.find(FIND_STRUCTURES, {
        filter: function (structure)
        {
            return structure.structureType === STRUCTURE_CONTAINER;
        }
    });
}

/**
 * returns all containers in the room and also the storage structure, if there is one
 *
 * @param room the room to search
 * @returns {*} an array containing all containers in the room
 */
export function roomContainersAndStorage(room: Room) : (StructureContainer | StructureStorage)[]
{
    return <(StructureContainer | StructureStorage)[]>room.find(FIND_STRUCTURES, {
        filter: function (structure)
        {
            return structure.structureType === STRUCTURE_CONTAINER || structure.structureType === STRUCTURE_STORAGE;
        }
    });
}

/**
 * returns how much energy is stored across all container and storage structures in the room
 *
 * @param room the room to search
 * @returns {*} how much energy is stored across all container and storage structures in the room
 */
export function storedEnergy(room: Room): number
{
    const roomStorage = room.storage;
    if (!roomStorage)
        return 0;

    return roomContainers(room).reduce((function (sum, container)
        {
            return sum + container.store.energy;
        }), 0) +
        roomStorage.store.energy;
}

/**
 * finds structures within the room that should be repaired
 * this uses the room's repairSettings:
 *    maxDefenseHealth: highest health to repair walls and ramparts to
 *    wallRepairThreshold: requires the room to have at least this much energy in containers and storage in order to repair walls and ramparts
 *
 * @param room the room to search
 * @returns {*} structures in the room that should be repaired
 */
export function structuresNeedingRepair(room: Room) : Structure[]
{
    const damagedStructures:Structure[] = room.find(FIND_STRUCTURES, {
        filter: function (structure)
        {
            //if the structure has an owner, and that owner isnt us, then skip it
            if(<OwnedStructure>structure && (<OwnedStructure>structure).owner && !(<OwnedStructure>structure).my)
                return false;

            //only include structures which can be damaged (ex: this excludes controllers)
            if (!structure.hits)
                return false;

            //only include damaged structures
            if (structure.hits >= structure.hitsMax)
                return false;

            //if wallRepairThreshold is set, only repair walls if at least that much energy is in storage
            if (structure.structureType === STRUCTURE_WALL)
                if (room.memory.repairSettings.wallRepairThreshold)
                    if (storedEnergy(room) < room.memory.repairSettings.wallRepairThreshold)
                        return false;

            //don't repair walls or ramparts above the max defense health set for the room
            if (structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART)
                if (structure.hits > room.memory.repairSettings.maxDefenseHealth)
                    return false;

            return true;
        }
    });

    if (room.memory.logSettings.damagedStructureCount)
        console.log(Game.time + " [" + room.name + "] " + damagedStructures.length + " structures in need of repair");

    return damagedStructures;
}

/**
 * returns whether or not the room has a structure of the given type
 *
 * @param room the room to search
 * @param structureType the structure type to search for
 * @returns {boolean}
 */
export function hasStructure(room: Room, structureType: StructureConstant)
{
    const roomStructures = room.find(FIND_STRUCTURES);
    return roomStructures.some(function (structure)
    {
        return structure.structureType === structureType;
    });
}

/**
 * returns whether or not the room has any hostile units or structures
 *
 * @param room
 */
export function hasHostiles(room: Room): boolean
{
    return ( (room.find(FIND_HOSTILE_CREEPS).length > 0) || (room.find(FIND_HOSTILE_STRUCTURES).length > 0) );
}

/**
 * returns where creeps in this room should idle
 */
export function idlePos(room: Room): RoomPosition
{
    const flags = room.find(FIND_FLAGS);
    const idleFlag = _.find(flags, flag => flag.color === COLOR_ORANGE && flag.secondaryColor === COLOR_GREY);

    if(idleFlag)
        return idleFlag.pos;
    else
        return new RoomPosition(25, 25, room.name);
}
